/*encoding:utf-8*/
// filename:  Main
// author:    1
// created:   2022/11/6の8:27
// project:   jvLearning
// idea:      PhpStorm
package cn.edu.jcu.coder.code;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;
import javafx.scene.control.TreeView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import java.io.File;
import java.io.InputStream;
import java.util.Objects;
import java.util.Map;
import java.util.List;
import java.net.HttpURLConnection;
import java.net.URL;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import cn.edu.jcu.coder.code.tool.RequestHelper;
import cn.edu.jcu.coder.code.tool.JsonHelper;
import cn.edu.jcu.coder.code.DbUtil;


class Btn extends Button{
    private StringProperty p0 = new  SimpleStringProperty("");
    private StringProperty p89 = new SimpleStringProperty("");
    private StringProperty p90 = new SimpleStringProperty("");
    private StringProperty p92 = new SimpleStringProperty("");
    private StringProperty p93 = new SimpleStringProperty("");
    private StringProperty p95 = new SimpleStringProperty("");
    private StringProperty p97 = new SimpleStringProperty("");
    private StringProperty province = new SimpleStringProperty("");

    public final void setAll(){
    String a="{\"code\": 0,\"msg\": \"成功\",\"data\": {\"p0\": null,\"p89\": null,\"ct\": \"2022-05-01 12:05\",\"p90\": null,\"p92\": null,\"p93\": null,\"prov\": \"上海\",\"p95\": null,\"p97\": null}}";
    setAll(a);
    }
    public final void setAll(String properties){
    JsonHelper j=new JsonHelper();
    @SuppressWarnings("unchecked")
    Map<String,Map<String,String>> data = (Map<String,Map<String,String>>) j.parse(properties);
    Map<String,String> values= data.get("data");
    p0.set("0号柴油价格："+values.get("p0"));
    p89.set("89号汽油价格："+values.get("p89"));
    p90.set("90号汽油价格："+values.get("p90"));
    p92.set("92号汽油价格："+values.get("p92"));
    p93.set("93号汽油价格："+values.get("p93"));
    p95.set("95号汽油价格："+values.get("p95"));
    p97.set("97号汽油价格："+values.get("p97"));
    province.set(values.get("prov"));
    }

    public   StringProperty p0(){return  p0;}
    public  StringProperty p89(){return  p89;}
    public  StringProperty p90(){return  p90;}
    public  StringProperty p92(){return  p92;}
    public  StringProperty p93(){return  p93;}
    public  StringProperty p95(){return  p95;}
    public  StringProperty p97(){return  p97;}
    public  StringProperty province(){return  province;}
}



public class Main extends Application{
    public final String apiKey = "需要在https://www.qqlykm.cn/申请";
    public final String oilApi = "https://qqlykm.cn/api/oilprice/get?key="+apiKey+"&name=";
    public final String dreamApi = "https://qqlykm.cn/api/zgjm/get?key="+apiKey+"&keywords=";
    public final String qrCodeApi = "https://qqlykm.cn/api/qrcode/get?";//text=内容或网址&size=Number
    public static void main(String[] arguments){
    Application.launch(arguments);
    //    launch(arguments);
    }


    @Override
    public void start(Stage rootStage){

    ImageView imageView=getImage("api");
    imageView.setStyle("-fx-opacity:0.5;");
    reSize(rootStage,imageView);

    StackPane stackPane = new StackPane();
    // 需要布局
    stackPane.getChildren().add(imageView);
//    stackPane.getChildren().add(QRCodeVBox());

//    HBox hbox = new HBox(33.3);
//    hbox.getChildren().addAll(OilVBox(),QRCodeVBox(),DreamVBox());

    VBox box1=  OilVBox();
    VBox box2=  DreamVBox();
    VBox box3=  QRCodeVBox();


    TabPane tabPane = new TabPane();
    Tab tab1=new Tab("某省油价查询");tab1.setClosable(false);tab1.setContent(box1);tabPane.getTabs().add(tab1);
    Tab tab2=new Tab("周公解梦");tab2.setClosable(false);tab2.setContent(box2);tabPane.getTabs().add(tab2);
    Tab tab3=new Tab("二维码生成器");tab3.setClosable(false);tab3.setContent(box3);tabPane.getTabs().add(tab3);
    tabPane.getSelectionModel().select(tab3);

    stackPane.getChildren().add(tabPane);



    Scene scene = new Scene(stackPane,200,260);
    //todo：CSS没写
//    scene.getStylesheets().add("cn/edu/jcu/coder/code/fx.css");
    reSize(rootStage,imageView);
    rootStage.setTitle("小组作业");
    rootStage.setScene(scene);
    rootStage.show();
    }



    public VBox OilVBox(){

        VBox vbox = new VBox();
        HBox hbox = new HBox();

        Btn button = new Btn();
        button.setText("今日查询");
        TextField searchField = new TextField();
        hbox.getChildren().addAll(searchField,button);
    /*    searchField.textProperty().addListener(new ChangeListener<String>(){
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue){

            }
        });*/


        Label provinceLabel = new Label();
        Label p0 = new Label();
        Label p89 = new Label();
        Label p90 = new Label();
        Label p92 = new Label();
        Label p93 = new Label();
        Label p95 = new Label();
        Label p97 = new Label();

        p0.textProperty().bind(button.p0());
        p89.textProperty().bind(button.p89());
        p90.textProperty().bind(button.p90());
        p92.textProperty().bind(button.p92());
        p93.textProperty().bind(button.p93());
        p95.textProperty().bind(button.p95());
        p97.textProperty().bind(button.p97());

        button.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
            String keyword=searchField.getText();

            DbUtil db = new DbUtil("oil");

            RequestHelper r=new RequestHelper();
            JsonHelper j=new JsonHelper();
            String now=j.now();
            // 不对数据进行预处理(本来应该用Combo box实现而不是LineEdit)
            String json =db.getValue(keyword+now);
            String newResult ="";

            if (json == null){
                json=r.get(oilApi+r.urlEncode(keyword));
                db.setValue(keyword+now,r.urlEncode(json));

            }
            else{
                newResult="今天已经查过了~";
                json=r.urlDecode(json);
            }
            provinceLabel.setText(newResult+keyword+"今天的汽油价格为");

            button.setAll(json);
/*                        System.out.println(oilApi+searchField.getText()+"输入："+(json==null)+json);
                        System.out.println("["+oilApi+r.urlEncode(keyword)+"]输入："+json);
                        p0.setText(button.p0());
                        p89.setText(button.p89());
                        p90.setText(button.p90());
                        p92.setText(button.p92());
                        p93.setText(button.p93());
                        p95.setText(button.p95());
                        p97.setText(button.p97());*/
            }});

            vbox.getChildren().addAll(hbox,provinceLabel,p0 ,p89,p90,p92,p93,p95,p97);
            return vbox;
        }

    public VBox DreamVBox(){
        VBox vbox = new VBox();
        HBox hbox = new HBox();
        Button button = new Button();
        button.setText("周公解梦");
        TextField searchField = new TextField();
        Label provinceLabel = new Label();
        hbox.getChildren().addAll(searchField,button);
        TreeView<String> treeView = new TreeView<>();
        treeView.setStyle("-fx-opacity:0.5;");

        button.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
            String keyword=searchField.getText();
            TreeItem<String> rootItem = new TreeItem<>();
            DbUtil db = new DbUtil("dream");

            RequestHelper r=new RequestHelper();
            JsonHelper j=new JsonHelper();
            String now=j.now();
            String json =db.getValue(keyword+now);
            String newResult ="";

            if (json == null){
                json=r.get(dreamApi+r.urlEncode(keyword));
                db.setValue(keyword+now,r.urlEncode(json));

            }
            else{
                newResult="今天已经解过了~";
                json=r.urlDecode(json);
            }
            provinceLabel.setText(newResult+"梦见"+keyword+"可能是");
            @SuppressWarnings("unchecked")
            Map<String,Object> jsonDict =(Map<String,Object>) j.parse(json);
            @SuppressWarnings("unchecked")
            List<Map<String,String>> jsonList =(List<Map<String,String>>) jsonDict.get("data");

            TreeItem<String> title = new TreeItem<>();
            TreeItem<String> description = new TreeItem<>();

            for (Map<String,String> line:jsonList){
            title = new TreeItem<>(line.get("title"));
            description = new TreeItem<>(line.get("content"));
            title.getChildren().add(description);
            title.setExpanded(true);
            rootItem.getChildren().add(title);

            }
            rootItem.setExpanded(true);
            treeView.setRoot(rootItem);
            }});

            vbox.getChildren().addAll(hbox,provinceLabel,treeView);
            return vbox;

    }

    public VBox QRCodeVBox(){
        VBox vbox = new VBox();
        HBox hbox = new HBox();
        Button button = new Button();
        button.setText("生成二维码");
        TextField searchField = new TextField();
        Spinner<Integer> spinner = new Spinner<>(128,1024,256,1);  spinner.setEditable(true);
        Label provinceLabel = new Label();
        hbox.getChildren().addAll(searchField,spinner,button);

        ImageView imageView = new ImageView();

        button.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){


            DbUtil db = new DbUtil("qrcode");
            RequestHelper r=new RequestHelper();
            JsonHelper j=new JsonHelper();

            String keyword=searchField.getText();
            String size = Integer.toString(spinner.getValue());
            String imageFilename=keyword+"_"+size+".png";

            String selectKeyword=r.urlEncode(imageFilename);

            String filename =db.getValue(selectKeyword);
            String newResult ="";
            if (filename == null){
                filename=r.getImage(qrCodeApi+"text="+r.urlEncode(keyword)+"&size="+size,imageFilename);
                db.setValue(selectKeyword,r.urlEncode(filename));
            }
            else{
                newResult="这张图片已经保存在image文件夹了~";
                filename=r.urlDecode(filename);
            }


            provinceLabel.setText(newResult+"请扫码");

            Image imageFile =new Image(getClass().getResourceAsStream("image/"+filename));
            imageView.setImage(imageFile);
            imageView.setPreserveRatio(true);

            }});

            vbox.getChildren().addAll(hbox,provinceLabel,imageView);
            return vbox;
    }




    public ImageView getImage(String a){

        RequestHelper r=new RequestHelper();//请求类
        JsonHelper j=new JsonHelper();//解析类
        String api="https://qqlykm.cn/api/bing/get?type=rand";
        String resp = r.getImage(api,"背景图片.png");//盗链api，特别定制
        Image imageFile =new Image(getClass().getResourceAsStream("image/"+resp));
        ImageView imageView = new ImageView(imageFile);
        imageView.setId("backgroundImage");
        return imageView;
    }

    public void reSize(Stage primaryStage,ImageView imageView){

        primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
        @Override
        public void changed(ObservableValue<? extends Number> observable,Number oldValue,Number newValue){
        imageView.setFitHeight((double)newValue);
        }
        });

        primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
        @Override
        public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
            imageView.setFitWidth((double)newValue);
        }
        });

    }
}
