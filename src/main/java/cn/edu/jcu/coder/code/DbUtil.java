/*encoding:utf-8*/
// filename:  DbUtil
// author:
// created:   2022/11/6の21:14
// project:   jvLearning
// idea:      PhpStorm
package cn.edu.jcu.coder.code;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

import cn.edu.jcu.coder.code.tool.JsonHelper;

public class DbUtil {
    public static String tableName =null;



    public DbUtil(){
        tableName="test";
        try{
            createTable("test");
        }
        catch (Exception e){
            System.out.println("error:"+e.getMessage()+e);
        }
    }
    public DbUtil(String table){
        tableName=table;
        try{
            createTable(tableName);
        }
        catch (Exception e){
            System.out.println("error:"+e.getMessage()+e);
//        e.getMessage();
//        System.out.print("");
        }
    }

/*
    public static void main(String[] arguments){
        createTable();
        setValue("12","景德镇学院2"); setValue("32","4"); setValue("4","3");setValue("好","景德镇学");
//        getValue("0");getValue("11"); getValue("22"); getValue("33"); getValue("44");
//        System.out.println(getValue("1").equals(getValueJson("1")));
        getAll();
    }
*/
    public static void getValuePrint(String name ){
    getValuePrint(name,null);
    }
    public static void getValuePrint(String name ,String id){
        try {
            Connection  conn = getSQLiteConnection();

            Statement stmt = conn.createStatement();
            String selectSQL= "SELECT * FROM "+tableName+" where name='"+name+"';";
            if (id != null)selectSQL ="SELECT * FROM "+tableName+" where name='"+name+"' or id='"+id+"' ;";
            ResultSet rs = stmt.executeQuery( selectSQL );
            while (rs.next()) {
                System.out.print("查得：id:"+rs.getInt("id") + ",name:");
                System.out.println(rs.getString("name")+",value:"+rs.getString("value"));
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            System.out.println("error:"+e.getMessage());
        }
    }

    public static String getValue(String name){
//        String result ="{\"id\":1,\"name\":\"1\",\"value\":\"景德镇陶瓷大学\"}";
        JsonHelper j =new JsonHelper();
        String result =getValueJson(name);
        if (result ==null)return null;
        @SuppressWarnings("unchecked")
        Map<String,Object> object=(Map<String,Object>) j.parse(result);
        String value = (String)object.get("value");
        return value;
    }
    public static String getValueJson(String name){
        try {
            Connection  conn = getSQLiteConnection();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM "+tableName+" where name='"+name+"';" );
            while (rs.next()) {
                String result ="{\"id\":"+rs.getInt("id")+ ",\"name\":\""+rs.getString("name")+"\",\"value\":\""+rs.getString("value")+"\"}";
                rs.close();
                stmt.close();
                conn.close();
                return result;
            }
        } catch (Exception e) {
            System.out.println("error:"+e.getMessage());
        }
        return null;
    }
    public static void getAll(){
        try {
            Connection  conn = getSQLiteConnection();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM "+tableName+";" );
            while (rs.next()) {
                String result ="{\"id\":"+rs.getInt("id")+ ",\"name\":\""+rs.getString("name")+"\",\"value\":\""+rs.getString("value")+"\"}";
                System.out.println("->"+result);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            System.out.println("error:"+e.getMessage());
        }
    }

    public static void setValue(String name,String value){

        try{
            Connection  conn = getSQLiteConnection();
            Statement stmt =conn.createStatement();
            String sqlInsert = "insert into `"+tableName+"` ('id','name','value') values (null,'"+name+"','"+value+"');";
            String sqlUpdate = "update `"+tableName+"` set value=\""+value+"\" where name=\""+name+"\";";
            try{
                stmt.executeUpdate(sqlInsert);
                }
            catch (Exception e){
                stmt.executeUpdate(sqlUpdate);
                }
            stmt.close();
            conn.close();
        }
        catch (Exception e){
            System.out.println("\t数据库错误:"+e.getMessage());
        }
    }





//    private static String mysqlDriver = "com.mysql.cj.jdbc.Driver";

    public static void  createTable(String tableName){
        try{
            Connection conn=getSQLiteConnection();
            Statement stmt =conn.createStatement();

            String database=""+
            "create table "+tableName+"("+
            "id integer primary key autoincrement ,"+
            "name text unique,"+
            "value text default null"+
            ")";

            stmt.executeUpdate(database);
            stmt.close();
            conn.close();
        }
            catch (Exception e){
//            System.out.println("error:"+e.getMessage()+e);
            e.getMessage();
        }

    }

    public static Connection getSQLiteConnection() {
//    return getSQLiteConnection("database.sqlite");
    return getSQLiteConnection("cn/edu/jcu/coder/code/database.sqlite");
    }
    public static Connection getSQLiteConnection(String databaseName) {
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
//            conn=DriverManager.getConnection("jdbc:sqlite:database.sqlite");
            conn=DriverManager.getConnection("jdbc:sqlite:"+databaseName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
//
//    public static Connection getConnection(String driver, String url, String username, String password) {
//        Connection conn = null;
//        try {
//            Class.forName(driver);
//            conn = DriverManager.getConnection(url, username, password);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return conn;
//    }

//
//    public static Connection getMySQLConnection(String url, String username, String password) {
//        return getConnection(mysqlDriver, url, username, password);
//    }
//    public static void closeAll(Connection conn, PreparedStatement preparedStatement, ResultSet resultSet) {
//        try {
//            if (resultSet != null) {
//                resultSet.close();
//            }
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//            if (conn != null) {
//                conn.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    public static void closeAll(Connection conn, ResultSet resultSet) {
//        closeAll(conn, null, resultSet);
//    }
//    public static void closeAll(Connection conn, PreparedStatement preparedStatement) {
//        closeAll(conn, preparedStatement, null);
//    }
//    public static int executeUpdate(Connection conn, String sql, Object[] params) {
//        int result = 0;
//        PreparedStatement preparedStatement = null;
//        try {
//            preparedStatement = conn.prepareStatement(sql);
//            if (params != null) {
//                for (int i = 0; i < params.length; i++) {
//                    preparedStatement.setObject(i + 1, params[i]);
//                }
//            }
//            result = preparedStatement.executeUpdate();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeAll(conn, preparedStatement);
//        }
//        return result;
//    }
//    public static int executeUpdate(Connection conn, String sql) {
//        return executeUpdate(conn, sql, null);
//    }
//    public static ResultSet executeQuery(Connection conn, String sql, Object[] params) {
//        ResultSet result = null;
//        PreparedStatement preparedStatement = null;
//        try {
//            preparedStatement = conn.prepareStatement(sql);
//            if (params != null) {
//                for (int i = 0; i < params.length; i++) {
//                    preparedStatement.setObject(i + 1, params[i]);
//                }
//            }
//            result = preparedStatement.executeQuery();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//    public static ResultSet executeQuery(Connection conn, String sql) {
//        return executeQuery(conn, sql, null);
//    }
}