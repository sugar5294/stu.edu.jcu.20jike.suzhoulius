/*encoding:utf-8*/
// filename:  RequestHelper
// author:    1
// created:   2022/11/6の14:16
// project:   jvLearning
// idea:      PhpStorm
package cn.edu.jcu.coder.code.tool;
// 代码来源：https://gitee.com/sp42_admin/ajaxjs/tree/v1/aj-base/src/main/java/com/ajaxjs
// 代码用途：get->String   (网络请求)
//       ：getImage->String   (文件名)
//       :urlEncode/urlDecode
// 代码修改：使get请求可返回文件名或JsonString(以及其他一些大大小小修改)


import java.util.zip.GZIPInputStream;
import java.util.Base64;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.Set;
import java.util.Random;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.nio.file.StandardCopyOption;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

//import com.ajaxjs.util.map.MapTool;


public class RequestHelper{
//    public static void main(String[] arguments){
//
//    String resp = request.get("https://qqlykm.cn/api/bing/get");
////    String resp = request.getRedirectUrl("https://qqlykm.cn/api/bing/get");
////    String resp = request.getRedirectUrl("https://qqlykm.cn/api/free/ip/get");
////    String resp = request.get("https://qqlykm.cn/api/free/ip/get");
////    System.out.println(resp.substring(0,10));
//    System.out.println(resp);
//    }

    /*
    * url: https://qqlykm.cn/api/bing
    * return:JsonString/imageName
    */
    public static String get(String url){
     HttpBasicRequest request = new HttpBasicRequest();
     return request.get(url);
//     return null;
    }
    public static String getImage(String url,String imageName ){
     HttpBasicRequest request = new HttpBasicRequest();
     return request.getImage(url,false,null,imageName);
//     return null;
    }

    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
//			LOGGER.warning(e);
            System.out.println(e.getMessage());
            return null;
        }
    }
    public static String urlDecode(String str) {
        try {
            return URLDecoder.decode(str, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
//			LOGGER.warning(e);
            System.out.println(e.getMessage());
            return null;
        }
    }

}

/**
 * 流操作助手类
 *
 * @author sp42 frank@ajaxjs.com
 *
 */
class StreamHelper {
//	private static final LogHelper LOGGER = LogHelper.getLog(StreamHelper.class);

	/**
	 * 读输入的字节流转换到字符流，将其转换为文本（多行）的字节流转换为字符串
	 *
	 * @param in 输入流，无须手动关闭
	 * @return 字符串
	 */
	public static String byteStream2string(InputStream in) {
		return byteStream2string_Charset(in, StandardCharsets.UTF_8);
	}

	/**
	 * 读输入的字节流转换到字符流，将其转换为文本（多行）的字节流转换为字符串。可指定字符编码
	 *
	 * @param in     输入流，无须手动关闭
	 * @param encode 字符编码
	 * @return 字符串
	 */
	public static String byteStream2string_Charset(InputStream in, Charset encode) {
		StringBuilder result = new StringBuilder();

		// InputStreamReader 从一个数据源读取字节，并自动将其转换成 Unicode 字符
		// 相对地，OutputStreamWriter 将字符的 Unicode 编码写到字节输出流
		try (InputStreamReader inReader = new InputStreamReader(in, encode);
				/*
				 * Decorator，装饰模式，又称为 Wrapper，使它具有了缓冲功能 BufferedInputStream、BufferedOutputStream
				 * 只是在这之前动态的为它们加上一些功能（像是缓冲区功能）
				 */
				BufferedReader reader = new BufferedReader(inReader);) {

			String line = null;
			while ((line = reader.readLine()) != null) { // 一次读入一行，直到读入 null 为文件结束
				// 指定编码集的另外一种方法 line = new String(line.getBytes(), encodingSet);
				result.append(line);
				result.append('\n');
			}

			inReader.close();
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return null;
		}

		return result.toString();
	}

	/**
	 * 1K 的数据块
	 */
	public static final int BUFFER_SIZE = 1024;

	/**
	 * 两端速度不匹配，需要协调 理想环境下，速度一样快，那就没必要搞流，直接一坨给弄过去就可以了 流的意思很形象，就是一点一滴的，不是一坨坨大批量的
	 * 带缓冲的一入一出 出是字节流，所以要缓冲（字符流自带缓冲，所以不需要额外缓冲） 请注意，改方法不会关闭流 close，你需要手动关闭
	 *
	 * @param in       输入流，无须手动关闭
	 * @param out      输出流
	 * @param isBuffer 是否加入缓冲功能
	 */
	public static void write(InputStream in, OutputStream out, boolean isBuffer) {
		int readSize; // 读取到的数据长度
		byte[] buffer = new byte[BUFFER_SIZE]; // 通过 byte 作为数据中转，用于存放循环读取的临时数据

		try {
			if (isBuffer) {
				try (OutputStream _out = new BufferedOutputStream(out);) {// 加入缓冲功能
					while ((readSize = in.read(buffer)) != -1) {
						_out.write(buffer, 0, readSize);
					}
				}
			} else {
				// 每次读 1KB 数据，将输入流数据写入到输出流中
				// readSize = in.read(buffer, 0, bufferSize);
				while ((readSize = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
					out.write(buffer, 0, readSize);
					// readSize = in.read(buffer, 0, bufferSize);
				}

				out.flush();
			}
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 使用内存操作流，读取二进制，也就是将流转换为内存的数据。 InputStream 转换到 byte[]. 从输入流中获取数据， 转换到 byte[]
	 * 也就是 in 转到内存。虽然大家可能都在内存里面了但还不能直接使用，要转换
	 *
	 * @param in 输入流
	 * @return 返回本实例供链式调用
	 */
	public static byte[] inputStream2Byte(InputStream in) {
		// 使用内存操作流，读取二进制
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			write(in, out, true);

			return out.toByteArray();
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * 送入的 byte[] 转换为输出流。可指定 byte[] 某一部分数据。 注意这函数不会关闭输出流，请记得在适当的时候将其关闭。
	 *
	 * @param out    输出流
	 * @param data   输入的数据
	 * @param off    偏移
	 * @param length 长度
	 */
	public static void bytes2output(OutputStream out, byte[] data, int off, int length) {
		bytes2output(out, data, true, off, length);
	}

	/**
	 * 送入的 byte[] 转换为输出流。可指定 byte[] 某一部分数据。 注意这函数不会关闭输出流，请记得在适当的时候将其关闭。
	 *
	 * @param out        输出流
	 * @param data       输入的数据
	 * @param isBuffered 是否需要缓冲
	 * @param off        偏移
	 * @param length     长度
	 */
	public static void bytes2output(OutputStream out, byte[] data, boolean isBuffered, int off, int length) {
		try {
			if (isBuffered)
				out = new BufferedOutputStream(out, BUFFER_SIZE);

			if (off == 0 && length == 0)
				out.write(data);
			else
				out.write(data, off, length);

			out.flush();
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
		}

	}

	/**
	 * 在字节数组中截取指定长度数组
	 *
	 * @param data   输入的数据
	 * @param off    偏移
	 * @param length 长度
	 * @return 指定 范围的字节数组
	 */
	public static byte[] subBytes(byte[] data, int off, int length) {
		byte[] bs = new byte[length];
		System.arraycopy(data, off, bs, 0, length);
		return bs;
	}

	/**
	 * 在字节数组里查找某个字节数组，找到返回&lt;=0，未找到返回-1
	 *
	 * @param data   被搜索的内容
	 * @param search 要搜索内容
	 * @param start  搜索起始位置
	 * @return 目标位置，找不到返回-1
	 */
	public static int byteIndexOf(byte[] data, byte[] search, int start) {
		int len = search.length;

		for (int i = start; i < data.length; i++) {
			int temp = i, j = 0;

			while (data[temp] == search[j]) {
				temp++;
				j++;

				if (j == len)
					return i;
			}
		}

		return -1;
	}

	/**
	 * 在字节数组里查找某个字节数组，找到返回&lt;=0，未找到返回-1
	 *
	 * @param data   被搜索的内容
	 * @param search 要搜索内容
	 * @return 目标位置，找不到返回-1
	 */
	public static int byteIndexOf(byte[] data, byte[] search) {
		return byteIndexOf(data, search, 0);
	}

	/**
	 * 合并两个字节数组
	 *
	 * @param a 数组a
	 * @param b 数组b
	 * @return 新合并的数组
	 */
	public static byte[] concat(byte[] a, byte[] b) {
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);

		return c;
	}
}



class Encode {
//	private static final LogHelper LOGGER = LogHelper.getLog(Encode.class);

	/**
	 * 字节转编码为 字符串（ UTF-8 编码）
	 *
	 * @param bytes 输入的字节数组
	 * @return 字符串
	 */
	public static String byte2String(byte[] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}

	/**
	 * 字符串转为 UTF-8 编码的字符串
	 *
	 * @param str 输入的字符串
	 * @return UTF-8 字符串
	 */
	public static String byte2String(String str) {
		return byte2String(str.getBytes());
	}

	/**
	 * 将 URL 编码的字符还原，默认 UTF-8 编码
	 *
	 * @param str 已 URL 编码的字符串
	 * @return 正常的 Java 字符串
	 */
	public static String urlDecode(String str) {
		try {
			return URLDecoder.decode(str, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * 将字符进行 URL 编码，默认 UTF-8 编码
	 *
	 * @param str 正常的 Java 字符串
	 *
	 * @return 已 URL 编码的字符串
	 */
	public static String urlEncode(String str) {
		try {
			return URLEncoder.encode(str, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
//			LOGGER.warning(e);
            System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * URL 网址的中文乱码处理。 如果 Tomcat 过滤器设置了UTF-8 那么这里就不用重复转码了
	 *
	 * @param str 通常是 URL 的 Query String 参数
	 * @return 中文
	 */
	public static String urlChinese(String str) {
		return byte2String(str.getBytes(StandardCharsets.ISO_8859_1));
	}

	/**
	 * BASE64 编码
	 *
	 * @param bytes 输入的字节数组
	 * @return 已编码的字符串
	 */
	public static String base64Encode(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	/**
	 * BASE64 编码
	 *
	 * @param str 待编码的字符串
	 * @return 已编码的字符串
	 */
	public static String base64Encode(String str) {
		return base64Encode(str.getBytes(StandardCharsets.UTF_8));
	}

	/**
	 * BASE64 解码，但以 Byte 形式返回
	 *
	 * @param str 待解码的字符串
	 * @return 已解码的 Byte
	 */
	public static byte[] base64DecodeAsByte(String str) {
		try {
			return Base64.getDecoder().decode(str);
		} catch (IllegalArgumentException e) {
//			LOGGER.warning("BASE64 解码失败", e);
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * BASE64 解码 这里需要强制捕获异常。
	 * 中文乱码：http://s.yanghao.org/program/viewdetail.php?i=54806
	 *
	 * @param str 待解码的字符串
	 * @return 已解码的字符串
	 */
	public static String base64Decode(String str) {
		byte[] b = base64DecodeAsByte(str);
		return b == null ? null : byte2String(b);
	}

	/**
	 * 获取 字符串 md5 哈希值
	 *
	 * @param str 输入的字符串
	 * @return MD5 摘要，返回32位大写的字符串
	 */
	public static String md5(String str) {
		return hash("MD5", str);
	}

	/**
	 * 生成字符串的 SHA1/SHA-256 哈希值
	 *
	 * @param hash 哈希算法，可以是 SHA1/SHA-256
	 * @param str  输入的内容
	 * @return 已哈希过的字符串
	 */
	private static String hash(String hash, String str) {
		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance(hash);
		} catch (NoSuchAlgorithmException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return null;
		}

		md.update(str.getBytes(StandardCharsets.UTF_8));

		// byte数组转化为16进制字符串输出。注意安卓环境下无此方法
		return bytesToHex(md.digest()).toLowerCase();
	};

	private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);

	/**
	 * byte[] 转化为16进制字符串输出
	 *
	 * @param bytes
	 * @return
	 */
	public static String bytesToHex(byte[] bytes) {
		byte[] hexChars = new byte[bytes.length * 2];

		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;

			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}

		return new String(hexChars, StandardCharsets.UTF_8);
	}

	/**
	 * 生成字符串的 SHA1 哈希值
	 *
	 * @param str 输入的字符串
	 * @return 字符串的 SHA1 哈希值
	 */
	public static String getSHA1(String str) {
		return hash("SHA1", str);
	}

	/**
	 * 生成字符串的 SHA2 哈希值
	 *
	 * @param str 输入的字符串
	 * @return 字符串的 SHA2 哈希值
	 */
	public static String getSHA256(String str) {
		return hash("SHA-256", str);
	}

}




/**
 * 发送基础的 HTTP 请求
 *
 * @author sp42 frank@ajaxjs.com
 *
 */
class HttpBasicRequest extends StreamHelper {
//	private static final LogHelper LOGGER = LogHelper.getLog(HttpBasicRequest.class);

	/**
	 * 设置请求方法
	 */
	public final static BiConsumer<HttpURLConnection, String> setMedthod = (conn, method) -> {
		try {
			conn.setRequestMethod(method);
		} catch (ProtocolException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
		}
	};

	/**
	 * 设置 cookies
	 */
	public final static BiConsumer<HttpURLConnection, Map<String, String>> setCookies = (conn, map) -> conn.addRequestProperty("Cookie", MapTool.join(map, ";"));

	/**
	 * 请求来源
	 */
	public final static BiConsumer<HttpURLConnection, String> setReferer = (conn, url) -> conn.addRequestProperty("Referer", url); // httpUrl.getHost()?

	/**
	 * 设置超时 （单位：秒）
	 */
	public final static BiConsumer<HttpURLConnection, Integer> setTimeout = (conn, timeout) -> conn.setConnectTimeout(timeout * 1000);

	/**
	 * 设置客户端识别
	 */
	public final static BiConsumer<HttpURLConnection, String> setUserAgent = (conn, url) -> conn.addRequestProperty("User-Agent", url);

	/**
	 * 默认的客户端识别
	 */
	public final static Consumer<HttpURLConnection> setUserAgentDefault = conn -> setUserAgent.accept(conn,
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0");
//		    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

	/**
	 * HttpURLConnection 工厂函数，对输入的网址进行初始化
	 *
	 * @param url 请求目的地址
	 * @return 请求连接对象
	 */
	public static HttpURLConnection initHttpConnection(String url) {
		URL httpUrl = null;

		try {
			httpUrl = new URL(url);
		} catch (MalformedURLException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning(e, "初始化连接出错！URL[{0}]格式不对！", url);
		}

		try {
			return (HttpURLConnection) httpUrl.openConnection();
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning(e, "初始化连接出错！URL[{0}]。", url);
		}

		return null;
	}

	/**
	 * 发送请求，返回响应信息
	 *
	 * @param conn         请求连接对象
	 * @param isEnableGzip 是否需要 GZip 解码
	 * @param callback     回调里面请记得关闭 InputStream
	 * @return 可指定返回类型
	 */
	public static <T> T getResponse(HttpURLConnection conn, Boolean isEnableGzip, Function<InputStream, T> callback) {
		try {
			int responseCode = conn.getResponseCode();
			InputStream in = responseCode >= 400 ? conn.getErrorStream() : conn.getInputStream();// 发起请求，接收响应

			// 是否启动 GZip 请求
			// 有些网站强制加入 Content-Encoding:gzip，而不管之前的是否有 GZip 的请求
			boolean isGzip = isEnableGzip || "gzip".equals(conn.getHeaderField("Content-Encoding"));

			if (isGzip)
				in = new GZIPInputStream(in);

//			if (responseCode >= 400) {// 如果返回的结果是400以上，那么就说明出问题了
//				RuntimeException e = new RuntimeException(responseCode < 500 ? responseCode + "：客户端请求参数错误！" : responseCode + "：抱歉！我们服务端出错了！");
//				LOGGER.warning(e);
//			}

			if (callback == null) {
				in.close();
			} else
				return callback.apply(in);
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * 设置启动 GZip 请求
	 */
	public final static Consumer<HttpURLConnection> setGizpRequest = conn -> conn.addRequestProperty("Accept-Encoding", "gzip, deflate");

	/////////////////////// --------- GET -------------///////////////////////

	/**
	 * 简单 GET 请求（原始 API 版），返回文本。
	 *
	 * @param url 请求目标地址
	 * @return 响应内容（如 HTML，JSON 等）
	 */
	public static String simpleGET(String url) {
		try {
			return byteStream2string(new URL(url).openStream());
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning(e);
			return null;
		}
	}

	/**
	 * GET 请求，返回文本内容
	 *
	 * @param url 请求目标地址
	 * @return 响应的文本内容
	 */
	public static String get(String url) {
		return get(url, false);
	}

	/**
	 * GET 请求，返回文本内容
	 *
	 * @param url    请求目标地址
	 * @param isGzip true 表示为带 GZip 压缩
	 * @return 响应的文本内容
	 */
	public static String get(String url, boolean isGzip) {
		return get(url, isGzip, null);
	}

	/**
	 * GET 请求，返回文本内容
	 *
	 * @param url    请求目标地址
	 * @param isGzip true 表示为带 GZip 压缩
	 * @return 响应的文本内容
	 */
/*
	public static String get(String url, boolean isGzip, Consumer<HttpURLConnection> fn) {
		HttpURLConnection conn = initHttpConnection(url);

		if (isGzip)
			setGizpRequest.accept(conn);

		if (fn != null)
			fn.accept(conn);

		return getResponse(conn, isGzip, StreamHelper::byteStream2string);
	}
*/

////////////////////////////////////////////////////////////////
//  增加的内容   ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

    public static String getRedirectUrl(String url){


		HttpURLConnection conn = initHttpConnection(url);

        conn.setInstanceFollowRedirects(false);
        conn.setConnectTimeout(5*1000);
        Map<String,List<String>> map = conn.getHeaderFields();
        Set<String> stringSet = map.keySet();
//        for (String str:stringSet)System.out.println(str+"--"+conn.getHeaderField(str));
        String redirect =conn.getHeaderField("Location");
        conn.disconnect();
        return redirect;
    }

	public static String getImage(String url, boolean isGzip, Consumer<HttpURLConnection> fn,String imageName) {
	        String redirect =getRedirectUrl(url);
			HttpURLConnection conn = initHttpConnection(url);
            if (isGzip)
                setGizpRequest.accept(conn);

            if (fn != null)
                fn.accept(conn);

            String saveDir="cn/edu/jcu/coder/code/image/";
            if (redirect!=null){
                // 图片就返回文件名(bing壁纸)
    //            String saveDir="../image/";


                int pre=redirect.indexOf("id=")+3;
                int suf=redirect.indexOf("&");
                String fileName=redirect.substring(pre,suf);
                File f =new File(saveDir+fileName);
                //存在文件名就直接返回
//                System.out.println("文件："+saveDir+fileName);
                if (f.exists()) return fileName;

                return getResponse(conn, isGzip, initDownload2disk_Callback(saveDir, fileName) );
            }
            File f =new File(saveDir+imageName);
            if (f.exists()) return imageName;
            return getResponse(conn, isGzip, initDownload2disk_Callback(saveDir, imageName) );

	}
	public static String get(String url, boolean isGzip, Consumer<HttpURLConnection> fn) {

		HttpURLConnection conn = initHttpConnection(url);

		if (isGzip)
			setGizpRequest.accept(conn);

		if (fn != null)
			fn.accept(conn);


		return getResponse(conn, isGzip, StreamHelper::byteStream2string);
	}

	public static Function<InputStream, String> initDownload2disk_Callback(String saveDir, String fileName) {
		return in -> {
			File file = FileHelper.createFile(saveDir, fileName);

			try (OutputStream out = new FileOutputStream(file);) {
				write(in, out, true);
//				LOGGER.info("文件 [{0}]写入成功", file.toString());

//				return file.toString();
				return fileName;// 壁纸不需要路径信息
			} catch (IOException e) {
//				LOGGER.warning(e);
				System.out.println(e.getMessage());
			} finally {
				try {
					in.close();
				} catch (IOException e) {
//					LOGGER.warning(e);
					System.out.println(e.getMessage());
				}
			}

			return null;
		};
	}



////////////////////////////////////////////////////////////////
//  结束   ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


	/////////////////////// --------- POST -------------///////////////////////

	/**
	 * POST 请求
	 *
	 * @param url  请求目标地址
	 * @param data 表单数据 KeyValue的请求数据，注意要进行 ? &amp; 编码，使用
	 *
	 *             <pre>
	 *             URLEncoder.encode()
	 *             </pre>
	 *
	 * @return 携带请求信息的 Bean
	 */
	public static String post(String url, Map<String, Object> data) {
		if (data != null && data.size() > 0) {
			return post(url, MapTool.join(data, v -> v == null ? null : Encode.urlEncode(v.toString())));
		} else {
			return null;
		}
	}

	/**
	 * POST 请求
	 *
	 * @param url    请求目标地址
	 * @param params 字符串类型的请求数据，例如
	 *
	 *               <pre>
	 * ip=35.220.250.107&amp;verifycode=
	 *               </pre>
	 *
	 * @return 请求之后的响应的内容
	 */
	public static String post(String url, String params) {
		return post(url, params.getBytes(), null);
	}

	/**
	 * POST 请求
	 *
	 * @param url 请求目标地址
	 * @param b   请求数据
	 * @param fn  对 Conn 进行配置的函数
	 * @return 请求之后的响应的内容
	 */
	public static String post(String url, byte[] b, Consumer<HttpURLConnection> fn) {
		return post(url, b, fn, null);
	}

	public static String post(String url, String params, Consumer<HttpURLConnection> fn) {
		return post(url, params.getBytes(), fn);
	}

	/**
	 * POST 请求
	 *
	 * @param url 请求目标地址
	 * @param b   字节格式的请求数据
	 * @param fn  对 Conn 进行配置的函数
	 * @return 请求之后的响应的内容
	 */
	public static String post(String url, byte[] b, Consumer<HttpURLConnection> fn, Function<InputStream, String> responseHandler) {
		HttpURLConnection conn = initHttpConnection(url);
		setMedthod.accept(conn, "POST");
		conn.setDoOutput(true); // for conn.getOutputStream().write(someBytes);
		conn.setDoInput(true);

		if (fn != null)
			fn.accept(conn);
		else
			setFormPost.accept(conn);

		try (OutputStream out = conn.getOutputStream();) {
			out.write(b); // 输出流写入字节数据
			out.flush();
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning("写入 post 数据时失败！[{0}]", e);
		}

		return getResponse(conn, false, responseHandler == null ? StreamHelper::byteStream2string : responseHandler);
	}

	public static String put(String url, byte[] b, Consumer<HttpURLConnection> fn, Function<InputStream, String> responseHandler) {
		HttpURLConnection conn = initHttpConnection(url);
		setMedthod.accept(conn, "PUT");
		conn.setDoOutput(true); // for conn.getOutputStream().write(someBytes);
		conn.setDoInput(true);

		if (fn != null)
			fn.accept(conn);
		else
			setFormPost.accept(conn);

		try (OutputStream out = conn.getOutputStream();) {
			out.write(b); // 输出流写入字节数据
			out.flush();
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning("写入 post 数据时失败！[{0}]", e);
		}

		return getResponse(conn, false, responseHandler == null ? StreamHelper::byteStream2string : responseHandler);
	}

	public static String delete(String url, Consumer<HttpURLConnection> fn, Function<InputStream, String> responseHandler) {
		HttpURLConnection conn = initHttpConnection(url);
		setMedthod.accept(conn, "DELETE");
		conn.setDoOutput(true); // for conn.getOutputStream().write(someBytes);
		conn.setDoInput(true);

		if (fn != null)
			fn.accept(conn);

		return getResponse(conn, false, responseHandler == null ? StreamHelper::byteStream2string : responseHandler);
	}

	/**
	 * 设置 POST 方式
	 */
	public final static Consumer<HttpURLConnection> setFormPost = conn -> conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=utf-8");

}

/**
 * 文件操作工具类
 *
 * @author sp42 frank@ajaxjs.com
 *
 */

class FileHelper extends StreamHelper {
//	private static final LogHelper LOGGER = LogHelper.getLog(FileHelper.class);

	/**
	 * 当前系统的分隔符(linux、windows)
	 */
	public static final String SEPARATOR = File.separator;

	/**
	 * 复制文件
	 *
	 * @param target    源文件
	 * @param dest      目的文件/目录，如果最后一个为目录，则不改名，如果最后一个为文件名，则改名
	 * @param isReplace 是否替换已存在的文件，true = 覆盖
	 * @return true 表示复制成功
	 */
	public static boolean copy(String target, String dest, boolean isReplace) {
		try {
			if (isReplace)
				Files.copy(Paths.get(target), Paths.get(dest), StandardCopyOption.REPLACE_EXISTING);
			else
				Files.copy(Paths.get(target), Paths.get(dest));
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}

	/**
	 * 移动文件
	 *
	 * @param target 源文件
	 * @param dest   目的文件/目录，如果最后一个为目录，则不改名，如果最后一个为文件名，则改名
	 * @return 是否操作成功
	 */
	public static boolean move(String target, String dest) {
		try {
			Files.copy(Paths.get(target), Paths.get(dest));
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning(e);
			return false;
		}

		return true;
	}

	/**
	 * 删除文件或目录
	 *
	 * @param file 文件对象
	 */
	public static void delete(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();

			for (File f : files)
				delete(f);
		}

		if (!file.delete())
//			LOGGER.warning("文件 {0} 删除失败！", file.toString());
			System.out.println("文件"+file.toString()+"删除失败！");
	}

	/**
	 * 删除文件或目录
	 *
	 * @param filePath 文件的完全路径
	 */
	public static void delete(String filePath) {
		delete(new File(filePath));
	}

	/**
	 * 打开文件，返回其文本内容，可指定编码
	 *
	 * @param filePath 文件的完全路径
	 * @param encode   文件编码
	 * @return 文件内容
	 */
	public static String openAsText(String filePath, Charset encode) {
//		LOGGER.info("读取文件[{0}]", filePath);

		Path path = Paths.get(filePath);

		try {
			if (Files.isDirectory(path))
				throw new IOException("参数 fullpath：" + filePath + " 不能是目录，请指定文件");
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return null;
		}

		try {
			StringBuilder sb = new StringBuilder();
			Files.lines(path, encode).forEach(str -> sb.append(str));

			return sb.toString();
		} catch (IOException e) {
		    System.out.println(e.getMessage());
//			LOGGER.warning(e);
		}

		return null;
	}

	/**
	 * 打开文件，返回其文本内容。指定为 UTF-8 编码
	 *
	 * @param filePath 文件的完全路径
	 * @return 文件内容
	 */
	public static String openAsText(String filePath) {
		return openAsText(filePath, StandardCharsets.UTF_8);
	}

	/**
	 * 获得指定文件的 byte 数组
	 *
	 * @param file 文件对象
	 * @return 文件字节数组
	 */
	public static byte[] openAsByte(File file) {
		try {
			return inputStream2Byte(new FileInputStream(file));
		} catch (FileNotFoundException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * 保存文件数据
	 *
	 * @param file        文件对象
	 * @param data        文件数据
	 * @param isOverwrite 是否覆盖文件，true = 允许覆盖
	 */
	public static void save(File file, byte[] data, boolean isOverwrite) {
//		LOGGER.info("正在保存文件" + file);

		try {
			if (!isOverwrite && file.exists())
				throw new IOException(file + "文件已经存在，禁止覆盖！");

			if (file.isDirectory())
				throw new IOException(file + " 不能是目录，请指定文件");

			if (!file.exists())
				file.createNewFile();

			Files.write(file.toPath(), data);
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 保存文件数据
	 *
	 * @param file 文件对象
	 * @param data 文件数据
	 * @param off  偏移
	 * @param len  长度
	 */
	public static void save(File file, byte[] data, int off, int len) {
		try (OutputStream out = new FileOutputStream(file)) {
			bytes2output(out, data, false, off, len);
		} catch (IOException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 保存文本内容
	 *
	 * @param file 文件对象
	 * @param text 文本内容
	 */
	public static void saveText(File file, String text) {
/*
		if (Version.isDebug) {
			String _text = text.length() > 200 ? text.substring(0, 200) + "..." : text;
			LOGGER.info("正在保存文件{0}， 保存内容：\n{1}", file.toString(), _text);
		} else
			LOGGER.info("正在保存文件{0}， 保存内容：\n{1}", file.toString());
*/

		save(file, text.getBytes(StandardCharsets.UTF_8), true);
	}

	/**
	 * 保存文本内容
	 *
	 * @param filePath 文件路径
	 * @param text     文本内容
	 */
	public static void saveText(String filePath, String text) {
		saveText(new File(filePath), text);
	}

	/**
	 * 创建目录
	 *
	 * @param folder 目录字符串
	 */
	public static void mkDir(String folder) {
		File _folder = new File(folder);
		if (!_folder.exists())// 先检查目录是否存在，若不存在建立
			_folder.mkdirs();

		_folder.mkdir();
	}

	/**
	 * 根据文件名创建目录。 先剥离文件名，剩下的就是目录名。 如果没有输出目录则先创建。
	 *
	 * @param filePath 完整路径，最后一个元素为文件名
	 */
	public static void mkDirByFileName(String filePath) {
		String[] arr = filePath.split("\\/|\\\\");
		arr[arr.length - 1] = "";// 取消文件名，让最后一个元素为空字符串
		String folder = String.join(SEPARATOR, arr);

		mkDir(folder);
	}

	/**
	 * 检测文件所在的目录是否存在，如果没有则建立。可以跨多个未建的目录
	 *
	 * @param file 必须是文件，不是目录
	 */
	public static void initFolder(File file) {
		if (file.isDirectory())
			throw new IllegalArgumentException("参数必须是文件，不是目录");

		mkDir(file.getParent());
	}

	/**
	 * 检测文件所在的目录是否存在，如果没有则建立。可以跨多个未建的目录
	 *
	 * @param file 必须是文件，不是目录
	 */
	public static void initFolder(String file) {
		initFolder(new File(file));
	}

	/**
	 * 新建一个空文件
	 *
	 * @param folder   如果路径不存在则自动创建
	 * @param fileName 保存的文件名
	 * @return 新建文件的 File 对象
	 */
	public static File createFile(String folder, String fileName) {
//		LOGGER.info("正在新建文件 {0}", folder + SEPARATOR + fileName);

		mkDir(folder);
		return new File(folder + SEPARATOR + fileName);
	}

	/**
	 * 创建文件，注意这是一个空的文件。如果没有指定目录则创建；检测是否可以覆盖文件
	 *
	 * @param filePath    文件完整路径，最后一个元素是文件名
	 * @param isOverwrite 是否覆盖文件
	 * @return 文件对象
	 * @throws IOException 文件已经存在
	 */
	public static File createFile(String filePath, boolean isOverwrite) throws IOException {
//		LOGGER.info("正在新建文件 {0}", filePath);

		mkDirByFileName(filePath);

		File file = new File(filePath);
		if (!isOverwrite && file.exists())
			throw new IOException("文件已经存在，禁止覆盖！");

		return file;
	}

	/**
	 * 根据日期字符串得到目录名 格式: /2008/10/15/
	 *
	 * @return 如 /2008/10/15/ 格式的字符串
	 */
	public static String getDirNameByDate() {
		String datatime = CommonUtils.now("yyyy-MM-dd"), year = datatime.substring(0, 4), mouth = datatime.substring(5, 7), day = datatime.substring(8, 10);

		return SEPARATOR + year + SEPARATOR + mouth + SEPARATOR + day + SEPARATOR;
	}

	/**
	 * 输入 /foo/bar/foo.jpg 返回 foo.jpg
	 *
	 * @param str 输入的字符串
	 * @return 文件名
	 */
	public static String getFileName(String str) {
		String[] arr = str.split("\\/|\\\\");// 取消文件名，让最后一个元素为空字符串

		return arr[arr.length - 1];
	}

	/**
	 * 获取 URL 上的文件名，排除 ? 参数部分
	 *
	 * @param url URL
	 * @return 文件名
	 */
	public static String getFileNameFromUrl(String url) {
		return getFileName(url).split("\\?")[0];
	}

	/**
	 * 獲取文件名的擴展名
	 *
	 * @param filename 文件名
	 * @return 擴展名
	 */
	public static String getFileSuffix(String filename) {
		return filename.substring(filename.lastIndexOf(".") + 1);
	}
}


/**
 * 普通未分类的工具类
 *
 * @author sp42 frank@ajaxjs.com
 *
 */
class CommonUtils {
	/**
	 * 是否空字符串
	 *
	 * @param str 要判断的字符串
	 * @return true 表示为为空字符串，否则不为空
	 */
	public static boolean isEmptyString(String str) {
		return str == null || str.isEmpty() || str.trim().isEmpty();
	}

	/**
	 * 将一个字符串分隔为字符串数组，分隔符 可以是,、/、-、\（注意转义）、|、; 作为分隔符。 常在读取配置的时候使用。
	 *
	 * @param str 输入的字符串
	 * @return 分隔后的数组
	 */
	public static String[] split(String str) {
		return str.split(",|/|-|\\\\|\\||;");
	}

	/**
	 * 重复字符串 repeat 次并以 div 分隔
	 *
	 * @param str    要重复的字符串
	 * @param div    字符串之间的分隔符
	 * @param repeat 重复次数
	 * @return 结果
	 */
	public static String repeatStr(String str, String div, int repeat) {
		StringBuilder s = new StringBuilder();
		int i = 0;

		while (i++ < repeat) {
			s.append(str);
			if (i != repeat)
				s.append(div);
		}

		return s.toString();
	}

	/**
	 * 输入 a，看 a 里面是否包含另一个字符串 b，忽略大小写。
	 *
	 * @param a 输入字符串 a
	 * @param b 另一个字符串 b
	 * @return true 表示包含
	 */
	public static boolean containsIgnoreCase(String a, String b) {
		return a.toLowerCase().contains(b.toLowerCase());
	}

	private static Matcher getMatcher(String regexp, String str) {
		return Pattern.compile(regexp).matcher(str);
	}
//
//	/**
//	 * 测试字符串是否正则
//	 *
//	 * @param regexp 正则
//	 * @param str    测试的字符串
//	 * @return true 表示匹配
//	 */
//	public static boolean regTest(String regexp, String str) {
//		return getMatcher(regexp, str).find();
//	}

	/**
	 * 使用正则的快捷方式
	 *
	 * @param regexp 正则
	 * @param str    测试的字符串
	 * @return 匹配结果，只有匹配第一个
	 */
	public static String regMatch(String regexp, String str) {
		return regMatch(regexp, str, 0);
	}

	/**
	 * 使用正则的快捷方式。可指定分组
	 *
	 * @param regexp     正则
	 * @param str        测试的字符串
	 * @param groupIndex 分组 id，若为 -1 则取最后一个分组
	 * @return 匹配结果
	 */
	public static String regMatch(String regexp, String str, int groupIndex) {
		Matcher m = getMatcher(regexp, str);
		return m.find() ? m.group(groupIndex == -1 ? m.groupCount() : groupIndex) : null;
	}

	/**
	 * 返回所有匹配项
	 *
	 * @param regexp 正则
	 * @param str    测试的字符串
	 * @return 匹配结果
	 */
	public static String[] regMatchAll(String regexp, String str) {
		Matcher m = getMatcher(regexp, str);
		List<String> list = new ArrayList<>();

		while (m.find()) {
			String g = m.group();
			list.add(g);
		}

		String[] result = list.toArray(new String[list.size()]);
		return result;
	}

	/**
	 * 判断数组是否为空
	 *
	 * @param arr 输入的数组
	 * @return true 表示为素组不是为空，是有内容的，false 表示为数组为空数组，length = 0
	 */
	public static boolean isNull(Object[] arr) {
		return arr == null || arr.length == 0;
	}

	/**
	 * 判断 collection 是否为空
	 *
	 * @param collection Map输入的集合
	 * @return true 表示为集合不是为空，是有内容的，false 表示为空集合
	 */
	public static boolean isNull(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	/**
	 * 判断 map 是否有意义
	 *
	 * @param map 输入的
	 * @return true 表示为 map 不是为空，是有内容的，false 表示为空 map
	 */
	public static boolean isNull(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

	/**
	 * 常见的日期格式
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_SHORT = "yyyy-MM-dd HH:mm";
	public static final String DATE_FORMAT_SHORTER = "yyyy-MM-dd";

	/**
	 * 对输入的时间进行格式化，采用格式 yyyy-MM-dd HH:mm:ss
	 *
	 * @param date 输入的时间
	 * @return 转换到 yyyy-MM-dd HH:mm:ss 格式的时间
	 */
	public static String formatDate(Date date) {
		return simpleDateFormatFactory(DATE_FORMAT).format(date);
	}

	/**
	 * 对输入的时间进行格式化，采用格式 YYYY-MM-dd HH:MM
	 *
	 * @param date 输入的时间
	 * @return 转换到 YYYY-MM-dd HH:MM 格式的时间
	 */
	public static String formatDateShorter(Date date) {
		return simpleDateFormatFactory(DATE_FORMAT_SHORT).format(date);
	}

	/**
	 * 返回当前时间，并对当前时间进行格式化
	 *
	 * @param format 期望的格式
	 * @return 转换到期望格式的当前时间
	 */
	public static String now(String format) {
		return simpleDateFormatFactory(format).format(new Date());
	}

	/**
	 * 返回当前时间的 YYYY-MM-dd HH:MM:ss 字符串类型
	 *
	 * @return 当前时间
	 */
	public static String now() {
		return now(DATE_FORMAT);
	}

	/**
	 * 年月日的正则表达式，例如 2016-08-18
	 */
	private final static String DATE_YEAR = "((19|20)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";

	/**
	 * 正则实例
	 */
	private final static Pattern DATE_YEAR_PATTERN = Pattern.compile(DATE_YEAR);

	/**
	 * 一般日期判断的正则
	 */
	private final static Pattern DATE_PATTERN = Pattern.compile(DATE_YEAR + " ([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]");

	/**
	 * 支持任意对象转换为日期类型
	 *
	 * @param obj 任意对象
	 * @return 日期类型对象，返回 null 表示为转换失败
	 */
	public static Date Objet2Date(Object obj) {
		if (obj == null)
			return null;

		else if (obj instanceof Date)
			return (Date) obj;
		else if (obj instanceof Long)
			return new Date((Long) obj);
		else if (obj instanceof Integer)
			return Objet2Date(Long.parseLong(obj + "000")); /* 10 位长 int，后面补充三个零为13位 long 时间戳 */
		else if (obj instanceof String) {
			String str = obj.toString();

			if (isEmptyString(str))
				return null;

			try {
				if (DATE_PATTERN.matcher(str).matches())
					return simpleDateFormatFactory(DATE_FORMAT).parse(str);
				else if (DATE_YEAR_PATTERN.matcher(str).matches())
					return simpleDateFormatFactory(DATE_FORMAT_SHORTER).parse(str);
				else
					return simpleDateFormatFactory(DATE_FORMAT_SHORT).parse(str);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// 输入日期不合法，不能转为日期类型。请重新输入日期字符串格式类型，或考虑其他方法。
		}

		return null;
	}

	/**
	 * SimpleDateFormat caches
	 */
	private final static Map<String, SimpleDateFormat> FORMATERS = new ConcurrentHashMap<>();

	/**
	 * 对输入的时间进行格式化 有 SimpleDateFormat 缓存 格式化的另外一种方法
	 *
	 * <pre>
	 * {@code
	 *   new SimpleDateFormat(format).format(System.currentTimeMillis());
	 *  }
	 * </pre>
	 *
	 * 返回 SimpleDateFormat 的工厂函数
	 *
	 * @param format 日期格式
	 * @return 格式日期的对象
	 */
	public static SimpleDateFormat simpleDateFormatFactory(String format) {
		if (!FORMATERS.containsKey(format))
			FORMATERS.put(format, new SimpleDateFormat(format));

		return FORMATERS.get(format);
	}

	/**
	 * 随机字符串
	 */
	private static final String STR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * 生成指定长度的随机字符，可能包含数字
	 *
	 * @param length 户要求产生字符串的长度
	 * @return 随机字符
	 */
	public static String getRandomString(int length) {
		Random random = new Random();
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < length; i++) {
			int number = random.nextInt(62);
			sb.append(STR.charAt(number));
		}

		return sb.toString();
	}
}

