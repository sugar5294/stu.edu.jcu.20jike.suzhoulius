/**
 * Copyright Sp42 frank@ajaxjs.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.edu.jcu.coder.code.tool;
// 代码来源：https://gitee.com/sp42_admin/ajaxjs/tree/v1/aj-base/src/main/java/com/ajaxjs
// 代码用途：parse->jsonString to List/Map
// 代码用途：now->今天的日期
// 代码修改：融合多个类，注释部分需要外部依赖的功能(xml)

import java.util.Stack;
import java.util.function.Consumer;
import java.util.HashMap;
import java.util.Objects;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Random;
import java.util.Date;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Collection;
import java.util.function.Function;
import java.util.concurrent.ConcurrentHashMap;

import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.UndeclaredThrowableException;
import java.lang.reflect.InvocationTargetException;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.BeanInfo;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactoryConfigurationError;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.InputStream;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.math.BigDecimal;

//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.w3c.dom.Element;
//import org.xml.sax.SAXException;
//import com.ajaxjs.util.logger.LogHelper;
//import com.ajaxjs.util.XmlHelper;
//import com.ajaxjs.util.CommonUtil;
//import com.ajaxjs.util.ReflectUtil;
//import com.ajaxjs.util.MappingValue;

/**
 * 反射工具包
 *
 * @author sp42 frank@ajaxjs.com
 */
class ReflectUtil {
//	private static final LogHelper LOGGER = LogHelper.getLog(ReflectUtil.class);

	/**
	 * 根据类创建实例，可传入构造器参数。
	 *
	 * @param clz  类对象
	 * @param args 获取指定参数类型的构造函数，这里传入我们想调用的构造函数所需的参数。可以不传。
	 * @return 对象实例
	 */
	public static <T> T newInstance(Class<T> clz, Object... args) {
		if (clz.isInterface()) {
//			LOGGER.warning("所传递的class类型参数为接口，无法实例化");
			System.out.println("所传递的class类型参数为接口，无法实例化");
			return null;
		}

		if (args == null || args.length == 0) {
			try {
				return clz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
//				LOGGER.warning(e);
				System.out.println(e.getMessage());
			}
		}

		// 获取构造器
		Constructor<T> constructor = getConstructor(clz, args2class(args));
		return newInstance(constructor, args);
	}

	/**
	 * 根据构造器创建实例
	 *
	 * @param constructor 类构造器
	 * @param args        获取指定参数类型的构造函数，这里传入我们想调用的构造函数所需的参数。可以不传。
	 * @return 对象实例
	 */
	public static <T> T newInstance(Constructor<T> constructor, Object... args) {
		try {
			return constructor.newInstance(args); // 实例化
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//			LOGGER.warning(e, "实例化对象失败：" + constructor.getDeclaringClass());
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * 传入的类是否有带参数的构造器
	 *
	 * @param clz 类对象
	 * @return true 表示为有带参数
	 */
	public static boolean hasArgsCon(Class<?> clz) {
		Constructor<?>[] constructors = clz.getConstructors();
		for (Constructor<?> constructor : constructors) {
			if (constructor.getParameterTypes().length != 0)
				return true;
		}

		return false;
	}
	/**
	 * 根据类全称创建实例，并转换到其接口的类型
	 *
	 * @param className 实际类的类型
	 * @param clazz     接口类型
	 * @return 对象实例
	 */
	// @SuppressWarnings("unchecked")
	// public static <T> T newInstance(String className, Class<T> clazz) {
	// Class<?> clz = getClassByName(className);
	// return clazz != null ? (T) newInstance(clz) : null;
	// }

	/**
	 * 根据类全称创建实例
	 *
	 * @param clzName 类全称
	 * @param args    根据构造函数，创建指定类型的对象,传入的参数个数需要与上面传入的参数类型个数一致
	 * @return 对象实例，因为传入的类全称是字符串，无法创建泛型 T，所以统一返回 Object
	 */
	public static Object newInstance(String clzName, Object... args) {
		Class<?> clazz = getClassByName(clzName);
		return clazz != null ? newInstance(clazz, args) : null;
	}

	/**
	 * 获取类的构造器，可以支持重载的构造器（不同参数的构造器）
	 *
	 * @param clz    类对象
	 * @param argClz 指定构造函数的参数类型，这里传入我们想调用的构造函数所需的参数类型
	 * @return 类的构造器
	 */
	public static <T> Constructor<T> getConstructor(Class<T> clz, Class<?>... argClz) {
		try {
			return argClz != null ? clz.getConstructor(argClz) : clz.getConstructor();
		} catch (NoSuchMethodException e) {
//			LOGGER.warning(e, "找不到这个 {0} 类的构造器。", clz.getName());
			System.out.println(e.getMessage());
		} catch (SecurityException e) {
//			LOGGER.warning(e);
			System.out.println(e.getMessage());
		}

		return null;
	}

	/**
	 * 根据类名字符串获取类对象
	 *
	 * @param clzName 类全称。如果是内部类请注意用法
	 * @return 类对象
	 */
	public static Class<?> getClassByName(String clzName) {
		try {
			return Class.forName(clzName);
		} catch (ClassNotFoundException e) {
//			LOGGER.warning(e, "找不到这个类：{0}。", clzName);
			System.out.println(e.getMessage());
		}

		return null;
	}

	/**
	 * 根据类名字符串获取类对象，可强类型转换类型
	 *
	 * @param clzName 类全称
	 * @param clz     要转换的目标类型
	 * @return 类对象
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getClassByName(String clzName, Class<T> clz) {
		Class<?> c = getClassByName(clzName);
		return c == null ? null : (Class<T>) getClassByName(clzName);
	}

	/**
	 * 把参数转换为类对象列表
	 *
	 * @param args 可变参数列表
	 * @return 类对象列表
	 */
	public static Class<?>[] args2class(Object[] args) {
		Class<?>[] clazzes = new Class[args.length];

		for (int i = 0; i < args.length; i++)
			clazzes[i] = args[i].getClass();

		return clazzes;
	}

	/**
	 * 已知接口类型，获取它的 class
	 *
	 * @param type 接口类型
	 * @return 接口的类对象
	 */
	public static Class<?> getClassByInterface(Type type) {
		String className = type.toString();
		className = className.replaceAll("<.*>$", "").replaceAll("(class|interface)\\s", ""); // 不要泛型的字符

		return getClassByName(className);
	}

	/**
	 * 获取某个类的所有接口
	 *
	 * @param clz 目标类
	 * @return 类的所有接口
	 */
	public static Class<?>[] getDeclaredInterface(Class<?> clz) {
		List<Class<?>> fields = new ArrayList<>();

		for (; clz != Object.class; clz = clz.getSuperclass()) {
			Class<?>[] currentInterfaces = clz.getInterfaces();
			fields.addAll(Arrays.asList(currentInterfaces));
		}

		return fields.toArray(new Class[fields.size()]);
	}

	/////////////// Methods ///////////////////////

	/**
	 * 根据类、方法的字符串和参数列表获取方法对象，支持重载的方法
	 *
	 * @param obj    可以是实例对象，也可以是类对象
	 * @param method 方法名称
	 * @param args   明确的参数类型列表
	 * @return 匹配的方法对象，null 表示找不到
	 */
	public static Method getMethod(Object obj, String method, Class<?>... args) {
		Class<?> cls = obj instanceof Class ? (Class<?>) obj : obj.getClass();

		try {
			return CommonUtil.isNull(args) ? cls.getMethod(method) : cls.getMethod(method, args);
		} catch (NoSuchMethodException | SecurityException e) {
			String str = "";
			for (Class<?> clz : args)
				str += clz.getName();
            System.out.println(e.getMessage());
//			LOGGER.warning("类找不到这个方法 {0}.{1}({2})。", cls.getName(), method, str.equals("") ? "void" : str);
			return null;
		}
	}

	/**
	 *
	 * 根据方法名称和参数列表查找方法。注意参数对象类型由于没有向上转型会造成不匹配而找不到方法，这时应使用上一个方法或
	 * getMethodByUpCastingSearch()
	 *
	 * @param obj    实例对象
	 * @param method 方法名称
	 * @param args   对应重载方法的参数列表
	 * @return 匹配的方法对象，null 表示找不到
	 */
	public static Method getMethod(Object obj, String method, Object... args) {
		if (!CommonUtil.isNull(args)) {
			return getMethod(obj, method, args2class(args));
		} else
			return getMethod(obj, method);
	}

	/**
	 * 根据方法名称和参数列表查找方法。自动循环参数类型向上转型。仅支持一个参数。
	 *
	 * @param clz    实例对象的类对象
	 * @param method 方法名称
	 * @param arg    参数对象，可能是子类或接口，所以要在这里找到对应的方法，当前只支持单个参数；且不能传 Class，必须为对象
	 * @return 匹配的方法对象，null 表示找不到
	 */
	public static Method getMethodByUpCastingSearch(Class<?> clz, String method, Object arg) {
		for (Class<?> clazz = arg.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				// return cls.getDeclaredMethod(methodName, clazz);
				return clz.getMethod(method, clazz); // 用 getMethod 代替更好？
			} catch (NoSuchMethodException | SecurityException e) {
				// 这里的异常不能抛出去。 如果这里的异常打印或者往外抛，则就不会执行clazz = clazz.getSuperclass(), 最后就不会进入到父类中了
			}
		}

		return null;
	}

	/**
	 * 循环 object 向上转型（接口）
	 *
	 * @param clz    主类
	 * @param method 方法名称
	 * @param arg    参数对象，可能是子类或接口，所以要在这里找到对应的方法，当前只支持单个参数
	 * @return 方法对象
	 */
	public static Method getDeclaredMethodByInterface(Class<?> clz, String method, Object arg) {
		Method methodObj = null;

		for (Class<?> clazz = arg.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
			Type[] intfs = clazz.getGenericInterfaces();

			if (intfs.length != 0) { // 有接口！
				try {
					for (Type intf : intfs) {
						// 旧方法，现在不行，不知道之前怎么可以的 methodObj = hostClazz.getDeclaredMethod(method,
						// (Class<?>)intf);
						// methodObj = cls.getMethod(methodName,
						// ReflectNewInstance.getClassByInterface(intf));
						methodObj = getSuperClassDeclaredMethod(clz, method, getClassByInterface(intf));

						if (methodObj != null)
							return methodObj;
					}
				} catch (Exception e) {
//					LOGGER.warning(e);
					System.out.println(e.getMessage());
				}
			} else {
				// 无实现的接口
			}
		}

		return null;
	}

	/**
	 * 查找对象父类身上指定的方法
	 *
	 * @param clz    主类
	 * @param method 方法名称
	 * @param argClz 参数类引用
	 * @return 匹配的方法对象，null 表示找不到
	 */
	public static Method getSuperClassDeclaredMethod(Class<?> clz, String method, Class<?> argClz) {
		for (; clz != Object.class; clz = clz.getSuperclass()) {
			try {
				return clz.getDeclaredMethod(method, argClz);
			} catch (NoSuchMethodException | SecurityException e) {
			}
		}

		return null;
	}

	/**
	 * 查找对象父类身上指定的方法（注意该方法不需要校验参数类型是否匹配，故有可能不是目标方法，而造成异常，请谨慎使用）
	 *
	 * @param clz    主类
	 * @param method 方法名称
	 * @return 匹配的方法对象，null 表示找不到
	 */
	public static Method getSuperClassDeclaredMethod(Class<?> clz, String method) {
		for (; clz != Object.class; clz = clz.getSuperclass()) {
			for (Method m : clz.getDeclaredMethods()) {
				if (m.toString().contains(method)) {
					return m;
				}
			}
		}

		return null;
	}

	/**
	 * 调用方法
	 *
	 * @param instance 对象实例，bean
	 * @param method   方法对象
	 * @param args     参数列表
	 * @return 执行结果
	 * @throws Throwable
	 */
	public static Object executeMethod_Throwable(Object instance, Method method, Object... args) throws Throwable {
		if (instance == null || method == null)
			return null;

		try {
			return args == null || args.length == 0 ? method.invoke(instance) : method.invoke(instance, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
			Throwable e;

			if (e1 instanceof InvocationTargetException) {
				e = ((InvocationTargetException) e1).getTargetException();
//				LOGGER.warning("反射执行方法异常！所在类[{0}] 方法：[{1}]", instance.getClass().getName(), method.getName());

				throw e;
			}

			throw e1;
		}
	}

	/**
	 * 获取实际抛出的那个异常对象。 InvocationTargetException 太过于宽泛，在 trouble
	 * shouting的时候，不能给人非常直观的信息 AOP 的缘故，不能直接捕获原来的异常，要不断 e.getCause()....
	 *
	 * @param e 异常对象
	 * @return 实际异常对象
	 */
	public static Throwable getUnderLayerErr(Throwable e) {
		while (e.getClass().equals(InvocationTargetException.class) || e.getClass().equals(UndeclaredThrowableException.class)) {
			e = e.getCause();
		}

		return e;
	}

	/**
	 * 获取实际抛出的那个异常对象，并去掉前面的包名。
	 *
	 * @param e 异常对象
	 * @return 实际异常对象信息
	 */
	public static String getUnderLayerErrMsg(Throwable e) {
		String msg = getUnderLayerErr(e).toString();

		return msg.replaceAll("^[^:]*:\\s?", "");
	}

	/**
	 * 调用方法，该方法不会抛出异常
	 *
	 * @param instance 对象实例，bean
	 * @param method   方法对象
	 * @param args     参数列表
	 * @return 执行结果
	 */
	public static Object executeMethod(Object instance, Method method, Object... args) {
		try {
			return executeMethod_Throwable(instance, method, args);
		} catch (Throwable e) {
			return null;
		}
	}

	/**
	 * 调用方法
	 *
	 * @param instnace 对象实例，bean
	 * @param method   方法对象名称
	 * @param args     参数列表
	 * @return 执行结果
	 */
	public static Object executeMethod(Object instnace, String method, Object... args) {
		// 没有方法对象，先找到方法对象。可以支持方法重载，按照参数列表
		Class<?>[] clazzes = args2class(args);
		Method methodObj = getMethod(instnace.getClass(), method, clazzes);

		return methodObj != null ? executeMethod(instnace, methodObj, args) : null;
	}

	/**
	 * 调用方法。 注意获取方法对象，原始类型和包装类型不能混用，否则得不到正确的方法， 例如 Integer 不能与 int 混用。 这里提供一个
	 * argType 的参数，指明参数类型为何。
	 *
	 * @param instnace 对象实例
	 * @param method   方法名称
	 * @param argType  参数类型
	 * @param argValue 参数值
	 * @return 执行结果
	 */
	public static Object executeMethod(Object instnace, String method, Class<?> argType, Object argValue) {
		Method m = getMethod(instnace, method, argType);
		if (m != null)
			return executeMethod(instnace, m, argValue);

		return null;
	}

	/**
	 * 执行静态方法
	 *
	 * @param method 方法对象
	 * @param args   方法参数列表
	 * @return 执行结果
	 */
	public static Object executeStaticMethod(Method method, Object... args) {
		if (isStaticMethod(method)) {
			try {
				return executeMethod_Throwable(new Object(), method, args);
			} catch (Throwable e) {
//				LOGGER.warning(e);
				System.out.println(e.getMessage());
			}
		} else {
//			LOGGER.warning("这不是一个静态方法：" + method);
			System.out.println("这不是一个静态方法：" + method);
		}

		return null;
	}

	/**
	 * 是否静态方法
	 *
	 * @param method 方法对象
	 * @return true 表示为静态方法
	 */
	public static boolean isStaticMethod(Method method) {
		return Modifier.isStatic(method.getModifiers());
	}

	// -----------------------------------------------------------------------------------------
	// --------------------------------------BeanUtils------------------------------------------
	// -----------------------------------------------------------------------------------------

	/**
	 * 将第一个字母大写
	 *
	 * @param str 字符串
	 * @return 字符串
	 */
	public static String firstLetterUpper(String str) {
		// return str.substring(0, 1).toUpperCase() + str.substring(1); // 另外一种写法
		return Character.toString(str.charAt(0)).toUpperCase() + str.substring(1);
	}

	/**
	 * 根据方法名称来截取属性名称，例如把 getter 的 getXxx() 转换为 xxx 的字段名
	 *
	 * @param method 方法名称
	 * @param action set|get
	 * @return 属性名称
	 */
	public static String getFieldName(String method, String action) {
		method = method.replace(action, "");
		return Character.toString(method.charAt(0)).toLowerCase() + method.substring(1);
	}

	/**
	 * 调用 bean 对象的 setter 方法
	 *
	 * @param bean  Bean 对象
	 * @param name  属性名称，前缀不要带 set
	 * @param value 要设置的属性值
	 */
	public static void setProperty(Object bean, String name, Object value) {
		String setMethodName = "set" + firstLetterUpper(name);

		Objects.requireNonNull(bean, bean + "执行：" + setMethodName + " 未发现类");
//		Objects.requireNonNull(value, bean + "执行：" + setMethodName + " 未发现参数 value");

		Class<?> clazz = bean.getClass();

		// 要把参数父类的也包括进来
		Method method = getMethodByUpCastingSearch(clazz, setMethodName, value);

		// 如果没找到，那就试试接口的……
		if (method == null)
			method = getDeclaredMethodByInterface(clazz, setMethodName, value);

		// 如果没找到，那忽略参数类型，只要匹配方法名称即可。这会发生在：由于被注入的对象有可能经过了 AOP 的动态代理，所以不能通过上述逻辑找到正确的方法
		if (method == null)
			method = getSuperClassDeclaredMethod(clazz, setMethodName);

		// 最终还是找不到
		Objects.requireNonNull(method, "找不到目标方法[" + clazz.getSimpleName() + "." + setMethodName + "(" + value.getClass().getSimpleName() +")]");

		executeMethod(bean, method, value);
	}

	/**
	 *
	 * @param clz
	 * @return
	 */
	public static Map<String, Integer> getConstantsInt(Class<?> clz) {
		Map<String, Integer> map = new HashMap<>();

		Field[] fields = clz.getDeclaredFields();
		Object instance = newInstance(clz);

		for (Field field : fields) {
			String descriptor = Modifier.toString(field.getModifiers());// 获得其属性的修饰
			if (descriptor.equals("public static final")) {
				try {
					map.put(field.getName(), (int) field.get(instance));
				} catch (IllegalArgumentException | IllegalAccessException e) {
//					LOGGER.warning(e);
					System.out.println(e.getMessage());
				}
			}
		}

		return map;
	}
}


/**
 * Map 转换工具
 *
 * @author sp42 frank@ajaxjs.com
 *
 */
class MapTool {
//	private static final LogHelper LOGGER = LogHelper.getLog(MapTool.class);

	// --------------------------------------------------------------------------------------------------
	// -----------------------------------------------Map转换---------------------------------------------
	// --------------------------------------------------------------------------------------------------

	/**
	 * Map 转换为 String
	 *
	 * @param map Map 结构，Key 必须为 String 类型
	 * @param div 分隔符
	 * @param fn  对 Value 的处理函数，返回类型 T
	 * @return Map 序列化字符串
	 */
	public static <T> String join(Map<String, T> map, String div, Function<T, String> fn) {
		String[] pairs = new String[map.size()];

		int i = 0;

		for (String key : map.keySet())
			pairs[i++] = key + "=" + fn.apply(map.get(key));

		return String.join(div, pairs);
	}

	public static <T> String join(Map<String, T> map, Function<T, String> fn) {
		return join(map, "&", fn);
	}

	public static <T> String join(Map<String, T> map, String div) {
		return join(map, div, v -> v == null ? null : v.toString());
	}

	public static <T> String join(Map<String, T> map) {
		return join(map, "&");
	}

	/**
	 * String[] 转换为 Map
	 *
	 * @param pairs 结对的字符串数组，包含 = 字符分隔 key 和 value
	 * @param fn    对 Value 的处理函数，返回类型 Object
	 * @return Map 对象
	 */
	public static Map<String, Object> toMap(String[] pairs, Function<String, Object> fn) {
		if (CommonUtil.isNull(pairs))
			return null;

		Map<String, Object> map = new HashMap<>();

		for (String pair : pairs) {
			if (!pair.contains("="))
				throw new IllegalArgumentException("没有 = 不能转化为 map");

			String[] column = pair.split("=");

			if (column.length >= 2)
				map.put(column[0], fn == null ? column[1] : fn.apply(column[1]));
			else
				map.put(column[0], "");// 没有 等号后面的，那设为空字符串
		}

		return map;
	}

	/**
	 * String[] 转换为 Map，key 与 value 分别一个数组
	 *
	 * @param columns 结对的键数组
	 * @param values  结对的值数组
	 * @param fn      对 Value 的处理函数，返回类型 Object
	 * @return Map 对象
	 */
	public static Map<String, Object> toMap(String[] columns, String[] values, Function<String, Object> fn) {
		if (CommonUtil.isNull(columns))
			return null;

		if (columns.length != values.length)
			throw new UnsupportedOperationException("两个数组 size 不一样");

		Map<String, Object> map = new HashMap<>();

		int i = 0;
		for (String column : columns)
			map.put(column, fn.apply(values[i++]));

		return map;
	}

	/**
	 * 判断 map 非空，然后根据 key 获取 value，若 value 非空则作为参数传入函数接口 s
	 *
	 * @param map 输入的map
	 * @param key map的键
	 * @param s   如果过非空，那么接着要做什么？在这个回调函数中处理。传入的参数就是map.get(key)的值
	 */
	public static <T> void getValue(Map<String, T> map, String key, Consumer<T> s) {
		if (map != null) {
			T value = map.get(key);
			if (value != null)
				s.accept(value);
		}
	}

	/**
	 * 万能 Map 转换器，为了泛型的转换而设的一个方法，怎么转换在 fn 中处理
	 *
	 * @param map 原始 Map，key 必须为 String 类型
	 * @param fn  转换函数
	 * @return 转换后的 map
	 */
	public static <T, K> Map<String, T> as(Map<String, K> map, Function<K, T> fn) {
		Map<String, T> _map = new HashMap<>();
		map.forEach((k, v) -> _map.put(k, v == null ? null : fn.apply(v)));

		return _map;
	}

	public static Map<String, Object> as(Map<String, String[]> map) {
		return as(map, arr -> MappingValue.toJavaValue(arr[0]));
	}

	// --------------------------------------------------------------------------------------------------
	// -----------------------------------------------Bean-----------------------------------------------
	// --------------------------------------------------------------------------------------------------

	@FunctionalInterface
	public static interface EachFieldArg {
		public void item(String key, Object value, PropertyDescriptor property);
	}

	/**
	 * 遍历一个 Java Bean
	 *
	 * @param bean Java Bean
	 * @param fn   执行的任务，参数有 key, value, property
	 */
	public static void eachField(Object bean, EachFieldArg fn) {
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());

			for (PropertyDescriptor property : beanInfo.getPropertyDescriptors()) {
				String key = property.getName();

				// 得到 property 对应的 getter 方法
				Method getter = property.getReadMethod();
				Object value = getter.invoke(bean);

				fn.item(key, value, property);
			}
		} catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//			LOGGER.warning(e);
            System.out.println(e.getMessage());
		}
	}

	/**
	 * Map 转为 Bean
	 *
	 * @param map         原始数据
	 * @param clz         实体 bean 的类
	 * @param isTransform 是否尝试转换值
	 * @return 实体 bean 对象
	 */
	public static <T> T map2Bean(Map<String, ?> map, Class<T> clz, boolean isTransform) {
		T bean = ReflectUtil.newInstance(clz);

		eachField(bean, (key, v, property) -> {
			try {
				if (map.containsKey(key)) {
					Object value = map.get(key);

					// null 是不会传入 bean 的
					if (value != null) {
						Class<?> t = property.getPropertyType(); // Bean 值的类型，这是期望传入的类型，也就 setter 参数的类型

						if (isTransform && t != value.getClass())  // 类型相同，直接传入；类型不相同，开始转换
							value = MappingValue.objectCast(value, t);

//						LOGGER.info("t:" + t);
//						LOGGER.info("v:" + value + " type: " + value.getClass());

						property.getWriteMethod().invoke(bean, value);
					}
				}

				// 子对象
				for (String mKey : map.keySet()) {
					if (mKey.contains(key + '_')) {
						Method getter = property.getReadMethod(), setter = property.getWriteMethod();// 得到对应的 setter 方法

						Object subBean = getter.invoke(bean);
						String subBeanKey = mKey.replaceAll(key + '_', "");

						if (subBean != null) {// 已有子 bean
							if (map.get(mKey) != null) // null 值不用处理
								ReflectUtil.setProperty(subBean, subBeanKey, map.get(mKey));
						} else { // map2bean
							Map<String, Object> subMap = new HashMap<>();
							subMap.put(subBeanKey, map.get(mKey));
							subBean = map2Bean(subMap, setter.getParameterTypes()[0], isTransform);
							setter.invoke(bean, subBean); // 保存新建的 bean
						}
					}
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				if(e instanceof IllegalArgumentException)
//					LOGGER.warning("[{0}]参数类型不匹配，输入值是[{1}]", key, v);
					System.out.println(e.getMessage());

//				LOGGER.warning(e);
			}
		});

		return bean;
	}

	/**
	 * map 转实体
	 *
	 * @param map 原始数据
	 * @param clz 实体 bean 的类
	 * @return 实体 bean 对象
	 */
	public static <T> T map2Bean(Map<String, ?> map, Class<T> clz) {
		return map2Bean(map, clz, false);
	}

	/**
	 * Bean 转为 Map
	 *
	 * @param bean 实体 bean 对象
	 * @return Map 对象
	 */
	public static <T> Map<String, Object> bean2Map(T bean) {
		Map<String, Object> map = new HashMap<>();

		eachField(bean, (k, v, property) -> {
			if (!k.equals("class")) // 过滤 class 属性
				map.put(k, v);
		});

		return map;
	}

	// --------------------------------------------------------------------------------------------------
	// -----------------------------------------------XML------------------------------------------------
	// --------------------------------------------------------------------------------------------------

/*	public static String beanToXml(Object bean) {
		return mapToXml(bean2Map(bean));
	}*/

	/**
	 *
	 * 将 Map 转换为 XML 格式的字符串
	 *
	 * @param data Map 类型数据
	 * @return XML 格式的字符串
	 */
/*
	public static String mapToXml(Map<String, ?> data) {
		Document doc = XmlHelper.initBuilder().newDocument();
		Element root = doc.createElement("xml");
		doc.appendChild(root);

		data.forEach((k, v) -> {
			String value = data.get(k).toString();
			if (value == null)
				value = "";

			Element filed = doc.createElement(k);
			filed.appendChild(doc.createTextNode(value.trim()));
			root.appendChild(filed);
		});

		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			try (StringWriter writer = new StringWriter();) {
				transformer.transform(new DOMSource(doc), new StreamResult(writer));
				String output = writer.getBuffer().toString(); // .replaceAll("\n|\r", "");
				return output;
			}
		} catch (IOException | TransformerException | TransformerFactoryConfigurationError e) {
			LOGGER.warning(e);
		}

		return null;
	}
*/

	/**
	 * XML 格式字符串转换为 Map
	 *
	 * @param strXML XML 字符串
	 * @return XML 数据转换后的 Map
	 */
/*	public static Map<String, String> xmlToMap(String strXML) {
		if (strXML == null)
			return null;

		Map<String, String> data = new HashMap<>();

		try (InputStream stream = new ByteArrayInputStream(strXML.getBytes("UTF-8"));) {
			Document doc = XmlHelper.initBuilder().parse(stream);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getDocumentElement().getChildNodes();

			for (int idx = 0; idx < nodeList.getLength(); ++idx) {
				Node node = nodeList.item(idx);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					data.put(element.getNodeName(), element.getTextContent());
				}
			}

			return data;
		} catch (IOException | SAXException e) {
			LOGGER.warning(e);
			return null;
		}
	}*/
}



/**
 * 字符串 Token
 *
 * @author sp42 frank@ajaxjs.com
 */
class StringToken extends Token {
	/**
	 * 创建字符串 Token
	 *
	 * @param value 这是 JSON 字符串上的那个原始值。
	 */
	public StringToken(String value) {
		super(0, "STR", "字符串", null, value);
	}

	/**
	 * 转义
	 *
	 * @param str 输入的字符
	 * @return 转义后的结果
	 */
	public static String unescape(String str) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);

			if (c == '\\') {
				c = str.charAt(++i);// 游标前进一个字符

				switch (c) {
				case '"':
					sb.append('"');
					break;
				case '\\':
					sb.append('\\');
					break;
				case '/':
					sb.append('/');
					break;
				case 'b':
					sb.append('\b');
					break;
				case 'f':
					sb.append('\f');
					break;
				case 'n':
					sb.append('\n');
					break;
				case 'r':
					sb.append('\r');
					break;
				case 't':
					sb.append('\t');
					break;
				case 'u':
					String hex = str.substring(i + 1, i + 5);
					sb.append((char) Integer.parseInt(hex, 16));
					i += 4;
					break;
				default:
					throw new JsonParseException("“\\”后面期待“\"\\/bfnrtu”中的字符，结果得到“" + c + "”");
				}
			} else {
				sb.append(c);
			}
		}

		return sb.toString();
	}
}



/**
 * 数字 Token
 *
 * @author sp42 frank@ajaxjs.com
 */
class NumberToken extends Token {
	/**
	 * 创建数字 Token
	 *
	 * @param value 这是 JSON 字符串上的那个原始值。
	 */
	public NumberToken(String value) {
		super(1, "NUM", "数字", null, value);
	}
}



/**
 * 该类负责栈的实际操作
 *
 */
class Operator {
	/**
	 * 词法分析器
	 */
	private Lexer lex;

	/**
	 * 保存当前值的变量
	 */
	private Object curObj;

	/**
	 * 当前对象的值
	 */
	private Object curValue;

	/**
	 * 状态栈
	 */
	private Stack<State> statusStack = new Stack<>();

	/**
	 * 值栈
	 */
	private Stack<Object> keyStack = new Stack<>();

	/**
	 * MAP | LIST
	 */
	private Stack<Object> objStack = new Stack<>();

	/**
	 * 创建一个栈管理器
	 *
	 * @param lex 词法分析器
	 */
	public Operator(Lexer lex) {
		this.lex = lex;
	}

	/**
	 * 遇到对象，将其保存
	 *
	 * @param from		当前状态 id
	 * @param to 		下一个状态 id
	 * @param input 	输入 Token
	 * @return 下一个状态 id
	 */
	public State objs(State from, State to, Token input) {
		if (from != FMS.BGN)
			statusStack.push(from);

		curObj = new HashMap<Object, Object>();
		objStack.push(curObj);

		return to;
	}

	/**
	 * 遇到数组，将其保存
	 *
	 * @param from 		当前状态 id
	 * @param to 		下一个状态 id
	 * @param input 	输入 Token
	 * @return 下一个状态 id
	 */
	public State arrs(State from, State to, Token input) {
		if (from != FMS.BGN)
			statusStack.push(from);

		curObj = new ArrayList<Object>();
		objStack.push(curObj);

		return to;
	}

	/**
	 * 在状态栈里拿出一个状态来进行运算后，返回一个新的状态作为状态机的新状态
	 *
	 * @param from 	当前状态 id
	 * @param to 	下一个状态 id
	 * @param input 输入 Token
	 * @return 下一个状态 id
	 */
	@SuppressWarnings("unchecked")
	public State val(State from, State to, Token input) {
		if (input == Tokens.ARRE || input == Tokens.OBJE) {
			curObj = objStack.pop();
			curValue = curObj;
		} else if (input == Tokens.TRUE || input == Tokens.FALSE || input == Tokens.NIL || input.getType() == 0
				|| input.getType() == 1) {
			curValue = getRealValue(input);
		}

		if (statusStack.isEmpty()) {
			return FMS.EOF;
		} else {
			State s = statusStack.pop();

			if (s == FMS.ARRBV) {
				curObj = objStack.peek();
				((List<Object>) curObj).add(curValue);
				s = FMS.ARRAV;
			} else if (s == FMS.OBJBV) {
				curObj = objStack.peek();
				((Map<Object, Object>) curObj).put(keyStack.pop(), curValue);
				s = FMS.OBJAV;
			}

			return s;
		}
	}

	/**
	 * 对象的 key 入栈
	 *
	 * @param from 	当前状态 id
	 * @param to 	下一个状态 id
	 * @param input 输入 Token
	 * @return 下一个状态 id
	 */
	public State objak(State from, State to, Token input) {
		keyStack.push(getRealValue(input));

		return to;
	}

	/**
	 * 保存数组的元素
	 *
	 * @param from	当前状态 id
	 * @param to 	下一个状态 id
	 * @param input 输入 Token
	 * @return 下一个状态 id
	 */
	@SuppressWarnings("unchecked")
	public State arrav(State from, State to, Token input) {
		curValue = getRealValue(input);
		((List<Object>) curObj).add(curValue);

		return to;
	}

	/**
	 * 保存对象元素的 key 和 value
	 *
	 * @param from	 当前状态 id
	 * @param to	下一个状态 id
	 * @param input 输入 Token
	 * @return 下一个状态 id
	 */
	@SuppressWarnings("unchecked")
	public State objav(State from, State to, Token input) {
		curValue = getRealValue(input);
		((Map<Object, Object>) curObj).put(keyStack.pop(), curValue);

		return to;
	}

	/**
	 * 获取 Token 的值（Java 的值）
	 * 根据 value（从 JSON 得来的） 转换为 Java 可读取的值
	 * @param token 输入 Token
	 * @return Token 的值
	 */
	private Object getRealValue(Token token) {
		try {
			if (token == Tokens.TRUE || token == Tokens.FALSE || token == Tokens.NIL)
				return token.getJavaValue();
			else if (token instanceof StringToken)
				return StringToken.unescape(token.getValue());
			else if (token instanceof NumberToken) {
				// System.out.println(value.indexOf('.') != -1);
				// 奇葩问题
				// System.out.println(false ? Double.parseDouble(value) :
				// Integer.parseInt(value));
				if (token.getValue().indexOf('.') != -1) {
					return Double.parseDouble(token.getValue());
				} else {
					return Integer.parseInt(token.getValue());
				}
				// return value.indexOf('.') != -1 ? Double.parseDouble(value) :
				// Integer.parseInt(value);
			} else
				throw new JsonParseException("获取 Java 值失败！");

			// return input.toJavaValue();
		} catch (RuntimeException e) {
			lex.exceptionFactory("字符串转换错误", e);
			return null;
		}
	}

	/**
	 * 调用 Operator 身上的方法
	 *
	 * @param methodName 方法名称
	 * @return 方法对象
	 */
	static Method getMethod(String methodName) {
		try {
			return Operator.class.getMethod(methodName, new Class[] { State.class, State.class, Token.class });
		} catch (SecurityException | NoSuchMethodException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取当前对象
	 *
	 * @return 当前对象
	 */
	public Object getCurObj() {
		return curObj;
	}

	/**
	 * 获取当前字符值
	 *
	 * @return 当前字符值
	 */
	public Object getCurValue() {
		return curValue;
	}
}




/**
 * 状态常量表
 * @author sp42 frank@ajaxjs.com
 */
interface States {
	/**
	 * 目标状态转换操作列表
	 * BGN ARRBV ARRAV OBJBK OBJAK OBJBV OBJAV VAL EOF ERR
	 */

	/**
	 * 开始态
	 */
	public static final State BGN   = new State(0, "解析开始");

	/**
	 * 数组值前态
	 */
	public static final State ARRBV = new State(1, "数组待值");

	/**
	 * 数组值后态
	 */
	public static final State ARRAV = new State(2, "数组得值", Operator.getMethod("arrav"));

	/**
	 * 对象键前态
	 */
	public static final State OBJBK = new State(3, "对象待键");

	/**
	 * 对象键后态
	 */
	public static final State OBJAK = new State(4, "对象得键", Operator.getMethod("objak"));

	/**
	 * 对象值前态
	 */
	public static final State OBJBV = new State(5, "对象待值");

	/**
	 * 对象值后态
	 */
	public static final State OBJAV = new State(6, "对象得值", Operator.getMethod("objav"));

	/**
	 * 结果态
	 */
	public static final State VAL 	= new State(7, "得最终值", Operator.getMethod("val"));

	/**
	 * 结束态
	 */
	public static final State EOF 	= new State(8, "解析结束");

	/**
	 * 错误态
	 */
	public static final State ERR 	= new State(9, "异常错误");

	/**
	 * 状态矩阵
	 */
	public static final State[][] states = {
		/*INPUT——    STR NUM DESC SPLIT ARRS OBJS ARRE OBJE FALSE TRUE NIL BGN*/
		/* BGN */  { VAL, VAL, ERR, ERR, ARRBV, OBJBK, ERR, ERR, VAL, VAL, VAL, BGN },
		/* ARRBV */{ ARRAV, ARRAV, ERR, ERR, ARRBV, OBJBK, VAL, ERR, ARRAV, ARRAV, ARRAV, ERR },
		/* ARRAV */{ ERR, ERR, ERR, ARRBV, ERR, ERR, VAL, ERR, ERR, ERR, ERR, ERR },
		/* OBJBK */{ OBJAK, OBJAK, ERR, ERR, ERR, ERR, ERR, VAL, ERR, ERR, ERR, ERR },
		/* OBJAK */{ ERR, ERR, OBJBV, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR },
		/* OBJBV */{ OBJAV, OBJAV, ERR, ERR, ARRBV, OBJBK, ERR, ERR, OBJAV, OBJAV, OBJAV, ERR },
		/* OBJAV */{ ERR, ERR, ERR, OBJBK, ERR, ERR, ERR, VAL, ERR, ERR, ERR, ERR },
		/*VAL*/{},//没有后续状态,遇见此状态时弹出状态栈中的状态计算当前状态,占位，方便后期添加
		/*EOF*/{},//没有后续状态，占位，方便后期添加
		/*ERR*/{}//没有后续状态，占位，方便后期添加
	};
}


/**
 * 状态
 *
 * @author sp42 frank@ajaxjs.com
 */
class State {
	/**
	 * 创建一个状态对象
	 *
	 * @param index 		状态 id
	 * @param description	状态描述
	 */
	public State(int index, String description) {
		this.id = index;
		this.description = description;
	}

	/**
	 * 创建一个状态对象
	 *
	 * @param index			 状态 id
	 * @param description 	状态描述
	 * @param handler 		状态转换用的处理器
	 */
	public State(int index, String description, Method handler) {
		this(index, description);
		this.setHandler(handler);
	}

	/**
	 * 索引
	 */
	private int id;

	/**
	 * 状态转换操作
	 */
	private Method handler;

	/**
	 * 描述
	 */
	private String description;

	/**
	 * 下一步期望的描述
	 */
	private String expectDescription;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the expectDescription
	 */
	public String getExpectDescription() {
		return expectDescription;
	}

	/**
	 * @param expectDescription the expectDescription to set
	 */
	public void setExpectDescription(String expectDescription) {
		this.expectDescription = expectDescription;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the handler
	 */
	public Method getHandler() {
		return handler;
	}

	/**
	 * @param handler the handler to set
	 */
	public void setHandler(Method handler) {
		this.handler = handler;
	}
}


class BaseLexer {
	/**
	 * 是否空格字符
	 *
	 * @param c 传入的字符
	 * @return 是否空格字符
	 */
	public static boolean isSpace(char c) {
		return c == ' ' || c == '\t' || c == '\n';
	}

	/**
	 * 是否字符或者下划线
	 *
	 * @param c 传入的字符
	 * @return 是否字符或者下划线
	 */
	public static boolean isLetterUnderline(char c) {
		return (c >= 'a' && c <= 'z') || c == '_';
	}

	/**
	 * 是否数字字符
	 *
	 * @param c 传入的字符
	 * @return 是否数字字符
	 */
	public static boolean isNum(char c) {
		return c >= '0' && c <= '9';
	}

	/**
	 * 是否数字字符或小数
	 *
	 * @param c 传入的字符
	 * @return 是否数字字符或小数
	 */
	public static boolean isDecimal(char c) {
		return isNum(c) || c == '.';
	}

	/**
	 * 是否字符或者数字或下划线
	 *
	 * @param c 传入的字符
	 * @return 是否字符或者数字或下划线
	 */
	public static boolean isNumLetterUnderline(char c) {
		return isLetterUnderline(c) || isNum(c) || c == '_';
	}

}


/**
 * 基础模型类 请注意不要使用 int 而是使用 Integer
 *
 * @author sp42 frank@ajaxjs.com
 *
 */
class BaseModel implements Serializable {
	private static final long serialVersionUID = -5313880199638314543L;

	private Long id;

	/**
	 * 唯一 id
	 */
	private Long uid;

	/**
	 * 数据字典：状态
	 */
	private Integer stat;

	/**
	 * 设置数据字典：状态
	 *
	 * @param stat 状态
	 */
	public void setStat(Integer stat) {
		this.stat = stat;
	}

	/**
	 * 获取数据字典：状态
	 *
	 * @return 数据字典：状态
	 */
	public Integer getStat() {
		return stat;
	}

	// @NotBlank(message="名称不能为空")
	// @Size(min = 2, max = 255, message = "长度应该介于3和255之间")
	private String name;

	// @Size(max = 60000)
	private String content;

	/**
	 * 创建日期
	 */
	private Date createDate;

	/**
	 * 修改日期
	 */
	private Date updateDate;

	/**
	 * 封面图片
	 */
	private String cover;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	/**
	 * 扩展字段
	 */
	public Map<String, Object> extractData;

	public Map<String, Object> getExtractData() {
		return extractData;
	}

	public void setExtractData(HashMap<String, Object> extractData /* 若为 Map 不能进行反射，即使强类型也不行 */) {
		this.extractData = extractData;
	}

	public int getExtractInt(String key) {
		Object obj = getExtractData().get(key);
		return obj != null ? (int) obj : 0;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}
}


/**
 * 词法分析器
 */
class Lexer extends BaseLexer {
	/**
	 * 当前行号
	 */
	private int lineNum;

	/**
	 * 用于记录每一行的起始位置
	 */
	private Stack<Integer> colMarks = new Stack<>();

	/**
	 * 用于报错的行游标
	 */
	private int startLine;

	/**
	 * 用于报错的列游标
	 */
	private int startCol;

	/**
	 * 当前字符游标
	 */
	private int cur = -1;
	/**
	 * 保存当前要解析的字符串
	 */
	private String str;

	/**
	 * 保存当前要解析的字符串的长度
	 */
	private int len;

	/**
	 * JsonLex构造函数
	 *
	 * @param str 要解析的字符串
	 */
	public Lexer(String str) {
		this.str = str;
		this.len = str.length();
		this.startLine = 0;
		this.startCol = 0;
		this.cur = -1;
		this.lineNum = 0;
		this.colMarks.push(0);
	}

	/**
	 * 获取当前游标所在的字符
	 *
	 * @return 当前字符
	 */
	public char getCurChar() {
		return cur >= len - 1 ? 0 : str.charAt(cur);
	}

	/**
	 * 检查字符串是否结束
	 */
	private void checkEnd() {
		if (cur >= len - 1)
			throw exceptionFactory("未预期的结束，字符串未结束");
	}

	// str \"(\\\"|[^\"])*\"
	// def [_a-zA-Z][_a-zA-Z0-9]*
	// num -?[0-9]+(\.[0-9]+)?
	// space [ \t\n]+
	/**
	 * 获取下一个Token的主函数 Next这个方法循环调用nextChar获取下一个字符，碰见某种类型的初始字符，就开始进入相应Token类型的处理函数中
	 * @return 最终返回Token类型的对象
	 */
	public Token next() {
		if (lineNum == 0) {
			lineNum = 1;
			return Tokens.BGN;
		}

		char c;
		while ((c = nextChar()) != 0) {
			startLine = lineNum;
			startCol = getColNum();

			if (c == '"' || c == '\'') {
				return new StringToken(getStrValue(c));
			} else if (isLetterUnderline(c)) {
				return getValueToken();
			} else if (isNum(c) || c == '-') {
				return new NumberToken(getNumValue());
			} else if (isSpace(c)) {
				continue;
			} else {
				return parseSymbol(c);
			}
		}

		if (c == 0)
			return Tokens.EOF;

		return null;
	}

	/**
	 * 获取字符串的值
	 *
	 * @param s 传入的字符
	 * @return 字符串的值
	 */
	private String getStrValue(char s) {
		int start = cur;
		char c;

		while ((c = nextChar()) != 0) {
			if (c == '\\') {// 跳过斜杠以及后面的字符
				c = nextChar();
			} else if (s == c) { // 遇到结束的 引号 结束了 返回这个字符串
				return str.substring(start + 1, cur);
			}
		}

		checkEnd();
		return null;
	}

	/**
	 * 获取数字的值
	 *
	 * @return 数字的值
	 */
	private String getNumValue() {
		int start = cur;
		char c;

		while ((c = nextChar()) != 0) {
			if (!isDecimal(c))
				return str.substring(start, revertChar());
		}

		checkEnd();
		return null;
	}

	/**
	 * 用来处理 true、false、null 和对象 key 的值
	 *
	 * @return true、false、null 的 Token
	 */
	private Token getValueToken() {
		int start = cur;
		char c;

		while ((c = nextChar()) != 0) {
			if (!isNumLetterUnderline(c)) {
				String value = str.substring(start, revertChar());

				if ("true".equals(value)) {
					return Tokens.TRUE;
				} else if ("false".equals(value)) {
					return Tokens.FALSE;
				} else if ("null".equals(value)) {
					return Tokens.NIL;
				} else {
					return new StringToken(value); // 对象的 KEY 提示：StringToken 的二义性，既可作对象的 KEY，又可作字符串
				}
			}
		}

		checkEnd();
		return null;
	}

	/**
	 * 解析符号
	 *
	 * @param c 字符
	 * @return Token
	 */
	private Token parseSymbol(char c) {
		switch (c) {
		case '[':
			return Tokens.ARRS;
		case ']':
			return Tokens.ARRE;
		case '{':
			return Tokens.OBJS;
		case '}':
			return Tokens.OBJE;
		case ',':
			return Tokens.SPLIT;
		case ':':
			return Tokens.DESC;
		default:
			return null;
		}
	}

	/**
	 * 获取下一个字节，同时进行 行、列 计数
	 *
	 * @return 下一个字节，结束时返回 0
	 */
	private char nextChar() {
		if (cur >= len - 1)
			return 0;

		++cur;
		char c = str.charAt(cur);

		if (c == '\n') { // 遇到换行，记录一下所在行数，用于调试
			lineNum++;
			colMarks.push(cur);
		}

		return c;
	}

	/**
	 * 撤回一个字节，同时进行 行、列 计数，返回撤回前的字符游标 为什么要撤回？因为要先获取下一个字节才能得知是什么情况，知道之后，撤回。
	 *
	 * @return 下一个字节，结束时返回0
	 */
	private int revertChar() {
		if (cur <= 0)
			return 0;

		int rcur = cur--;

		if (str.charAt(rcur) == '\n') {
			lineNum--;
			colMarks.pop();
		}

		return rcur;
	}

	/**
	 * 抛出一个 JsonParseException 异常
	 *
	 * @param msg	异常信息
	 * @return JsonParseException 异常
	 */
	public JsonParseException exceptionFactory(String msg) {
		return new JsonParseException(cur, startLine, startCol, msg);
	}

	/**
	 * 抛出一个 JsonParseException 异常
	 *
	 * @param msg	异常信息
	 * @param e 	异常对象
	 * @return JsonParseException 异常
	 */
	public JsonParseException exceptionFactory(String msg, Throwable e) {
		return new JsonParseException(cur, startLine, startCol, msg, e);
	}

	public int getLineNum() {
		return lineNum;
	}

	public int getColNum() {
		return cur - colMarks.peek();
	}

	public int getCur() {
		return cur;
	}

	public String getStr() {
		return str;
	}

	public int getLen() {
		return len;
	}
}



/**
 * Token 类型
 */
class Token {
	/**
	 * 创建一个 Token
	 *
	 * @param type   TokenId
	 * @param name   Token 名称
	 * @param cnName Token 名称（中文名）
	 */
	public Token(int type, String name, String cnName) {
		this.type = type;
		this.typeName = name;
		this.typeNameChinese = cnName;
	}

	/**
	 * 创建一个 Token
	 *
	 * @param type   TokenId
	 * @param name   Token 名称
	 * @param cnName Token 名称（中文名）
	 * @param value  Java 类型的值，通常是已知的
	 */
	public Token(int type, String name, String cnName, Object value) {
		this(type, name, cnName);
		this.javaValue = value;
	}

	/**
	 * 创建一个 Token
	 *
	 * @param type      TokenId
	 * @param name      Token 名称
	 * @param cnName    Token 名称（中文名）
	 * @param value     Java 类型的值，通常是已知的
	 * @param jsonValue JSON 值（String|Number）
	 */
	public Token(int type, String name, String cnName, Object value, String jsonValue) {
		this(type, name, cnName, value);
		this.value = jsonValue;
	}

	/**
	 * Token 类型，也可以理解为 TokenId、TokenIndex
	 */
	private int type;

	/**
	 * 类型名称
	 */
	private String typeName;

	/**
	 * 类型名称（中文版本）
	 */
	private String typeNameChinese;

	/**
	 * Token 值，一般指数字和字符串。这是 JSON 字符串上的那个原始值。
	 */
	private String value;

	/**
	 * Java 的值，由 value 转换而来。
	 */
	private Object javaValue;

//	/**
//	 * 根据 value（从 JSON 得来的） 转换为 Java 可读取的值
//	 *
//	 * @return Java 中的类型
//	 */
//	public Object toJavaValue() {
//		if (this == Tokens.TRUE || this == Tokens.FALSE || this == Tokens.NIL)
//			return getJavaValue();
//		else if (this instanceof StringToken)
//			return StringToken.unescape(value);
//		else if (this instanceof NumberToken) {
//			// System.out.println(value.indexOf('.') != -1);
//			// 奇葩问题
//			// System.out.println(false ? Double.parseDouble(value) :
//			// Integer.parseInt(value));
//			if (value.indexOf('.') != -1) {
//				return Double.parseDouble(value);
//			} else {
//				return Integer.parseInt(value);
//			}
//			// return value.indexOf('.') != -1 ? Double.parseDouble(value) :
//			// Integer.parseInt(value);
//		} else
//			throw new JsonParseException("获取 Java 值失败！");
//	}

	private static String strTpl = "[ %s | %s : %s]";

	@Override
	public String toString() {
		return type > 1 ? "[" + getTypeName() + "]" : String.format(strTpl, getTypeName(), getTypeNameChinese(), value);
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the javaValue
	 */
	public Object getJavaValue() {
		return javaValue;
	}

	/**
	 * @param javaValue the javaValue to set
	 */
	public void setJavaValue(Object javaValue) {
		this.javaValue = javaValue;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the typeNameChinese
	 */
	public String getTypeNameChinese() {
		return typeNameChinese;
	}

	/**
	 * @param typeNameChinese the typeNameChinese to set
	 */
	public void setTypeNameChinese(String typeNameChinese) {
		this.typeNameChinese = typeNameChinese;
	}
}



/**
 * 列出常见的 Token 类型。字符串类型 Token 和 数字类型 Token 两个比较特殊，须另外设类来表示。
 *
 * @author sp42 frank@ajaxjs.com
 */
interface Tokens {
	/**
	 * 注意：Token 0 为 字符串类型 Token，参见 StringToken 类，这里不写
	 */

	/**
	 * 注意：Token 1 为 数字类型 Token，参见 NumberToken 类，这里不写
	 */

	/**
	 * 对象的值
	 */
	public static final Token DESC = new Token(2, "DESC", ":");

	/**
	 * 多个元素之间的分隔符
	 */
	public static final Token SPLIT = new Token(3, "SPLIT", ",");

	/**
	 * 数组开始
	 */
	public static final Token ARRS = new Token(4, "ARRS", "[");

	/**
	 * 对象开始
	 */
	public static final Token OBJS = new Token(5, "OBJS", "{");

	/**
	 * 数组结束
	 */
	public static final Token ARRE = new Token(6, "ARRE", "]");

	/**
	 * 对象结束
	 */
	public static final Token OBJE = new Token(7, "OBJE", "}");

	/**
	 * FALSE 值
	 */
	public static final Token FALSE = new Token(8, "FALSE", "false", false);

	/**
	 * TRUE 值
	 */
	public static final Token TRUE = new Token(9, "TRUE", "true", true);

	/**
	 * NULL 空值
	 */
	public static final Token NIL = new Token(10, "NIL", "null", null);

	/**
	 * 开始
	 */
	public static final Token BGN = new Token(11, "BGN", "开始");

	/**
	 * 结束
	 */
	public static final Token EOF = new Token(12, "EOF", "结束");
}


/**
 * JSON 解析器专用异常类
 *
 * @author sp42 frank@ajaxjs.com
 */
class JsonParseException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * 创建一个 JSON 解析异常对象
	 *
	 * @param charNum charNum
	 * @param lineNum 错误所在的行数
	 * @param colNum  错误所在的列数
	 * @param message 异常信息
	 */
	public JsonParseException(int charNum, int lineNum, int colNum, String message) {
		this.charNum = charNum;
		this.colNum = colNum;
		this.lineNum = lineNum;
		this.desc = message;
	}

	/**
	 * 创建一个 JSON 解析异常对象
	 *
	 * @param charNum charNum
	 * @param lineNum 错误所在的行数
	 * @param colNum  错误所在的列数
	 * @param message 异常信息
	 * @param cause   异常对象
	 */
	public JsonParseException(int charNum, int lineNum, int colNum, String message, Throwable cause) {
		this(charNum, lineNum, colNum, message);
		this.cause = cause;
	}

	/**
	 * 创建一个 JSON 解析异常对象
	 *
	 * @param message 异常信息
	 */
	public JsonParseException(String message) {
		super(message);
	}

	private int charNum;

	private int lineNum;

	private int colNum;

	private String desc;

	private Throwable cause;

	@Override
	public String getMessage() {
		return "JsonParseException[char: " + charNum + ", line: " + lineNum + ", column: " + colNum + "]" + desc + (cause == null ? "" : cause.toString());
	}

	@Override
	public String toString() {
		return getMessage();
	}
}


/**
 * 语法解析的状态机 Thanks to blog.csdn.net/yimengqiannian/article/details/53701275
 */
class FMS implements States {
	/**
	 * 词法分析器实例
	 */
	private Lexer lex;

	/**
	 * 堆栈管理器
	 */
	private Operator opt;

	/**
	 * 当前状态
	 */
	private State status;

	/**
	 * 创建一个状态机
	 *
	 * @param json JSON 字符串
	 */
	public FMS(String json) {
		Objects.requireNonNull(json, "没有输入的 JSON 字符串。");

		lex = new Lexer(json);
		opt = new Operator(lex);
	}

	/**
	 * 开始解析 JSON 字符串
	 *
	 * @return MAP|LIST
	 */
	public Object parse() {
		status = BGN;
		State oldStatus = status; // 上一个状态

		Token tk;
		while ((tk = lex.next()) != Tokens.EOF) {
			if (tk == null)
				throw lex.exceptionFactory("发现不能识别的 token：" + lex.getCurChar());

			if (status == VAL || status == EOF || status == ERR)
				throw lex.exceptionFactory(String.format(strTpl, oldStatus.getDescription(), "结束", tk.toString()));

			oldStatus = status;
			status = states[oldStatus.getId()][tk.getType()];

			if (status == ERR) {
				String expectStr = ETS[oldStatus.getId()];
				throw lex.exceptionFactory(String.format(strTpl, oldStatus.getDescription(), expectStr, tk.toString()));
			}

			Method m = TKOL[tk.getType()];

			try {
				if (m != null) // 输入 Token 操作 有点像 js 的 call/apply
					status = (State) m.invoke(opt, oldStatus, status, tk);

				m = status.getHandler();

				if (m != null) // 目标状态操作
					status = (State) m.invoke(opt, oldStatus, status, tk);

			} catch (IllegalArgumentException e) {
				throw lex.exceptionFactory("【反射调用】传入非法参数", e);
			} catch (IllegalAccessException e) {
				throw lex.exceptionFactory("【反射调用】私有方法无法调用", e);
			} catch (InvocationTargetException e) {
				if (e.getTargetException() instanceof JsonParseException)
					throw (JsonParseException) e.getTargetException();
				else
					throw lex.exceptionFactory("运行时异常", e);
			}
		}

		return opt.getCurValue();
	}

	private final static String strTpl = "当前状态【 %s 】, 期待【 %s 】; 但却返回 %s";

	/**
	 * Token 输入操作列表
	 */
	/* INPUT —— STR NUM DESC SPLIT ARRS OBJS ARRE OBJE FALSE TRUE NIL BGN */
	private static final Method[] TKOL = { null, null, null, null, Operator.getMethod("arrs"),
			Operator.getMethod("objs"), null, null, null, null, null, null };

	// STR NUM DESC SPLIT ARRS OBJS ARRE OBJE FALSE TRUE NIL BGN
	final static String[] allText = { "字符串", "数字", Tokens.DESC.getTypeNameChinese(), Tokens.SPLIT.getTypeNameChinese(),
			Tokens.ARRS.getTypeNameChinese(), Tokens.OBJS.getTypeNameChinese(), Tokens.ARRE.getTypeNameChinese(),
			Tokens.OBJE.getTypeNameChinese(), Tokens.FALSE.getTypeNameChinese(), Tokens.TRUE.getTypeNameChinese(),
			Tokens.NIL.getTypeNameChinese(), Tokens.BGN.getTypeNameChinese(), };

	// BGN ARRBV ARRAV OBJBK OBJAK OBJBV OBJAV VAL EOF ERR
	final static String[] ETS = { getExpectStr(BGN.getId()), getExpectStr(ARRBV.getId()), getExpectStr(ARRAV.getId()),
			getExpectStr(OBJBK.getId()), getExpectStr(OBJAK.getId()), getExpectStr(OBJBV.getId()),
			getExpectStr(OBJAV.getId()), Tokens.EOF.getTypeNameChinese(), Tokens.EOF.getTypeNameChinese(),
			Tokens.EOF.getTypeNameChinese() };

	/**
	 * 获取期望 Token 描述字符串
	 *
	 * @param stateId 状态 id
	 * @return 期望 Token 描述字符串
	 */
	static String getExpectStr(int stateId) {
		State[] stateArr = States.states[stateId];

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < stateArr.length; i++) {
			State s = stateArr[i];
			if (s != ERR) {
				sb.append(allText[i]).append('|');
			}
		}

		return sb.length() == 0 ? null : sb.deleteCharAt(sb.length() - 1).toString();
	}
}


/**
 * 普通未分类的工具类
 *
 * @author sp42 frank@ajaxjs.com
 *
 */
class CommonUtil {
	/**
	 * 是否空字符串
	 *
	 * @param str 要判断的字符串
	 * @return true 表示为为空字符串，否则不为空
	 */
	public static boolean isEmptyString(String str) {
		return str == null || str.isEmpty() || str.trim().isEmpty();
	}

	/**
	 * 将一个字符串分隔为字符串数组，分隔符 可以是,、/、-、\（注意转义）、|、; 作为分隔符。 常在读取配置的时候使用。
	 *
	 * @param str 输入的字符串
	 * @return 分隔后的数组
	 */
	public static String[] split(String str) {
		return str.split(",|/|-|\\\\|\\||;");
	}

	/**
	 * 重复字符串 repeat 次并以 div 分隔
	 *
	 * @param str    要重复的字符串
	 * @param div    字符串之间的分隔符
	 * @param repeat 重复次数
	 * @return 结果
	 */
	public static String repeatStr(String str, String div, int repeat) {
		StringBuilder s = new StringBuilder();
		int i = 0;

		while (i++ < repeat) {
			s.append(str);
			if (i != repeat)
				s.append(div);
		}

		return s.toString();
	}

	/**
	 * 输入 a，看 a 里面是否包含另一个字符串 b，忽略大小写。
	 *
	 * @param a 输入字符串 a
	 * @param b 另一个字符串 b
	 * @return true 表示包含
	 */
	public static boolean containsIgnoreCase(String a, String b) {
		return a.toLowerCase().contains(b.toLowerCase());
	}

	private static Matcher getMatcher(String regexp, String str) {
		return Pattern.compile(regexp).matcher(str);
	}
//
//	/**
//	 * 测试字符串是否正则
//	 *
//	 * @param regexp 正则
//	 * @param str    测试的字符串
//	 * @return true 表示匹配
//	 */
//	public static boolean regTest(String regexp, String str) {
//		return getMatcher(regexp, str).find();
//	}

	/**
	 * 使用正则的快捷方式
	 *
	 * @param regexp 正则
	 * @param str    测试的字符串
	 * @return 匹配结果，只有匹配第一个
	 */
	public static String regMatch(String regexp, String str) {
		return regMatch(regexp, str, 0);
	}

	/**
	 * 使用正则的快捷方式。可指定分组
	 *
	 * @param regexp     正则
	 * @param str        测试的字符串
	 * @param groupIndex 分组 id，若为 -1 则取最后一个分组
	 * @return 匹配结果
	 */
	public static String regMatch(String regexp, String str, int groupIndex) {
		Matcher m = getMatcher(regexp, str);
		return m.find() ? m.group(groupIndex == -1 ? m.groupCount() : groupIndex) : null;
	}

	/**
	 * 返回所有匹配项
	 *
	 * @param regexp 正则
	 * @param str    测试的字符串
	 * @return 匹配结果
	 */
	public static String[] regMatchAll(String regexp, String str) {
		Matcher m = getMatcher(regexp, str);
		List<String> list = new ArrayList<>();

		while (m.find()) {
			String g = m.group();
			list.add(g);
		}

		String[] result = list.toArray(new String[list.size()]);
		return result;
	}

	/**
	 * 判断数组是否为空
	 *
	 * @param arr 输入的数组
	 * @return true 表示为素组不是为空，是有内容的，false 表示为数组为空数组，length = 0
	 */
	public static boolean isNull(Object[] arr) {
		return arr == null || arr.length == 0;
	}

	/**
	 * 判断 collection 是否为空
	 *
	 * @param collection Map输入的集合
	 * @return true 表示为集合不是为空，是有内容的，false 表示为空集合
	 */
	public static boolean isNull(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	/**
	 * 判断 map 是否有意义
	 *
	 * @param map 输入的
	 * @return true 表示为 map 不是为空，是有内容的，false 表示为空 map
	 */
	public static boolean isNull(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

	/**
	 * 常见的日期格式
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_SHORT = "yyyy-MM-dd HH:mm";
	public static final String DATE_FORMAT_SHORTER = "yyyy-MM-dd";

	/**
	 * 对输入的时间进行格式化，采用格式 yyyy-MM-dd HH:mm:ss
	 *
	 * @param date 输入的时间
	 * @return 转换到 yyyy-MM-dd HH:mm:ss 格式的时间
	 */
	public static String formatDate(Date date) {
		return simpleDateFormatFactory(DATE_FORMAT).format(date);
	}

	/**
	 * 对输入的时间进行格式化，采用格式 YYYY-MM-dd HH:MM
	 *
	 * @param date 输入的时间
	 * @return 转换到 YYYY-MM-dd HH:MM 格式的时间
	 */
	public static String formatDateShorter(Date date) {
		return simpleDateFormatFactory(DATE_FORMAT_SHORT).format(date);
	}

	/**
	 * 返回当前时间，并对当前时间进行格式化
	 *
	 * @param format 期望的格式
	 * @return 转换到期望格式的当前时间
	 */
	public static String now(String format) {
		return simpleDateFormatFactory(format).format(new Date());
	}

	/**
	 * 返回当前时间的 YYYY-MM-dd HH:MM:ss 字符串类型
	 *
	 * @return 当前时间
	 */
	public static String now() {
		return now(DATE_FORMAT);
	}

	/**
	 * 年月日的正则表达式，例如 2016-08-18
	 */
	private final static String DATE_YEAR = "((19|20)[0-9]{2})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";

	/**
	 * 正则实例
	 */
	private final static Pattern DATE_YEAR_PATTERN = Pattern.compile(DATE_YEAR);

	/**
	 * 一般日期判断的正则
	 */
	private final static Pattern DATE_PATTERN = Pattern.compile(DATE_YEAR + " ([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]");

	/**
	 * 支持任意对象转换为日期类型
	 *
	 * @param obj 任意对象
	 * @return 日期类型对象，返回 null 表示为转换失败
	 */
	public static Date Objet2Date(Object obj) {
		if (obj == null)
			return null;

		else if (obj instanceof Date)
			return (Date) obj;
		else if (obj instanceof Long)
			return new Date((Long) obj);
		else if (obj instanceof Integer)
			return Objet2Date(Long.parseLong(obj + "000")); /* 10 位长 int，后面补充三个零为13位 long 时间戳 */
		else if (obj instanceof String) {
			String str = obj.toString();

			if (isEmptyString(str))
				return null;

			try {
				if (DATE_PATTERN.matcher(str).matches())
					return simpleDateFormatFactory(DATE_FORMAT).parse(str);
				else if (DATE_YEAR_PATTERN.matcher(str).matches())
					return simpleDateFormatFactory(DATE_FORMAT_SHORTER).parse(str);
				else
					return simpleDateFormatFactory(DATE_FORMAT_SHORT).parse(str);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// 输入日期不合法，不能转为日期类型。请重新输入日期字符串格式类型，或考虑其他方法。
		}

		return null;
	}

	/**
	 * SimpleDateFormat caches
	 */
	private final static Map<String, SimpleDateFormat> FORMATERS = new ConcurrentHashMap<>();

	/**
	 * 对输入的时间进行格式化 有 SimpleDateFormat 缓存 格式化的另外一种方法
	 *
	 * <pre>
	 * {@code
	 *   new SimpleDateFormat(format).format(System.currentTimeMillis());
	 *  }
	 * </pre>
	 *
	 * 返回 SimpleDateFormat 的工厂函数
	 *
	 * @param format 日期格式
	 * @return 格式日期的对象
	 */
	public static SimpleDateFormat simpleDateFormatFactory(String format) {
		if (!FORMATERS.containsKey(format))
			FORMATERS.put(format, new SimpleDateFormat(format));

		return FORMATERS.get(format);
	}

	/**
	 * 随机字符串
	 */
	private static final String STR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * 生成指定长度的随机字符，可能包含数字
	 *
	 * @param length 户要求产生字符串的长度
	 * @return 随机字符
	 */
	public static String getRandomString(int length) {
		Random random = new Random();
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < length; i++) {
			int number = random.nextInt(62);
			sb.append(STR.charAt(number));
		}

		return sb.toString();
	}
}





/**
 * 处理值的一些相关函数
 *
 * @author sp42 frank@ajaxjs.com
 */
class MappingValue {
	/**
	 * 把字符串还原为 Java 里面的真实值，如 "true"--true,"123"--123,"null"--null
	 *
	 * @param value 字符串的值
	 * @return Java 里面的值
	 */
	public static Object toJavaValue(String value) {
		if (value == null)
			return null;

		value = value.trim();

		if ("".equals(value))
			return "";
		if ("null".equals(value))
			return null;

		if ("true".equalsIgnoreCase(value))
			return true;
		if ("false".equalsIgnoreCase(value))
			return false;

		// try 比较耗资源，先检查一下
		if (value.charAt(0) == '-' || (value.charAt(0) >= '0' && value.charAt(0) <= '9'))
			try {
				int int_value = Integer.parseInt(value);
				if ((int_value + "").equals(value)) // 判断为整形
					return int_value;
			} catch (NumberFormatException e) {// 不能转换为数字
				try {
					long long_value = Long.parseLong(value);
					if ((long_value + "").equals(value)) // 判断为整形
						return long_value;
				} catch (NumberFormatException e1) {
					if (value.matches("[0-9]{1,13}(\\.[0-9]*)?")) {
						return Double.parseDouble(value);
					}
				}
			}

		return value;
	}

	/**
	 * true/1、 字符串 true/1/yes/on 被视为 true 返回； false/0/null、字符串 false/0/no/off/null
	 * 被视为 false 返回；
	 *
	 * @param value 输入值
	 * @return true/false
	 */
	public static boolean toBoolean(Object value) {
		if (value == null)
			return false;

		if (value.equals(true) || value.equals(1))
			return true;

		if (value instanceof String) {
			String _value = (String) value;

			if (_value.equalsIgnoreCase("yes") || _value.equalsIgnoreCase("true") || _value.equals("1") || _value.equalsIgnoreCase("on"))
				return true;

			if (_value.equalsIgnoreCase("no") || _value.equalsIgnoreCase("false") || _value.equals("0") || _value.equalsIgnoreCase("off") || _value.equalsIgnoreCase("null"))
				return false;
		}

		return false;
	}

	/**
	 * 根据送入的类型作适当转换
	 *
	 * @param value 送入的值
	 * @param t     期待的类型
	 * @return 已经转换类型的值
	 */
	@SuppressWarnings("unchecked")
	public static Object objectCast(Object value, Class<?> t) {
		if (value == null)
			return null;
		else if (t == boolean.class || t == Boolean.class) // 布尔型
			value = toBoolean(value);
		else if (t == int.class || t == Integer.class) // 整形
			value = CommonUtil.isEmptyString(value.toString()) ? 0 : Integer.parseInt(value.toString());
		else if (t == int[].class || t == Integer[].class) {
			// 复数
			if (value instanceof String)
				value = stringArr2intArr((String) value, DIVER + "");
			else if (value instanceof List)
				value = intList2Arr((List<Integer>) value);

		} else if (t == long.class || t == Long.class)
			// LONG 型
			value = Long.valueOf((value == null || CommonUtil.isEmptyString(value.toString())) ? "0" : value.toString());
		else if (t == String.class) // 字符串型
			value = value.toString();
		else if (t == String[].class) {
			// 复数
			if (value instanceof ArrayList) {
				ArrayList<String> list = (ArrayList<String>) value;
				value = list.toArray(new String[list.size()]);
			} else if (value instanceof String) {
				String str = (String) value;
				value = str.split(DIVER + "");
			} else {
				// LOGGER.info("Bean 要求 String[] 类型，但实际传入类型：" +
				// value.getClass().getName());
			}
		} else if (t == Date.class)
			value = CommonUtil.Objet2Date(value);
		else if (t == BigDecimal.class) {
			if (value instanceof Integer)
				value = new BigDecimal((Integer) value);
			else if (value instanceof String) {
				BigDecimal b = new BigDecimal((String) value);
				b.setScale(2, BigDecimal.ROUND_DOWN);

				value = b;
			} else if (value instanceof Double)
				value = new BigDecimal(Double.toString((Double) value));
		}

		return value;
	}

	/**
	 * 用于数组的分隔符
	 */
	private static final char DIVER = ',';

	/**
	 * Integer[] 不能直接转 int[]，故设置一个函数专门处理之
	 *
	 * @param list 整形列表
	 * @return 整形数组
	 */
	private static int[] intList2Arr(List<Integer> list) {
		return newIntArray(list.size(), index -> list.get(index));
	}

	/**
	 * 当它们每一个都是数字的字符串形式，转换为整形的数组
	 *
	 * <pre>
	 * {@code
	 *   "1,2,3, ..." -- [1, 2, ...]
	 * }
	 * </pre>
	 *
	 * @param value 输入字符串
	 * @param diver 分隔符
	 * @return 整形数组
	 */
	private static int[] stringArr2intArr(String value, String diver) {
		String[] strArr = value.split(diver);

		return newIntArray(strArr.length, index -> Integer.parseInt(strArr[index].trim()));
	}

	/**
	 * 创建一个 int 数列，对其执行一些逻辑
	 *
	 * @param length 数列最大值
	 * @param fn     执行的逻辑
	 * @return 数组
	 */
	private static int[] newIntArray(int length, Function<Integer, Integer> fn) {
		int[] arr = new int[length];

		for (int i = 0; i < length; i++)
			arr[i] = fn.apply(i);

		return arr;
	}
}




/**
 * 序列化/反序列化 JSON
 *
 * @author sp42 frank@ajaxjs.com
 */
public class JsonHelper {

/*
    // // 测试
    public static void main(String[] arguments){
    String listString =new String("[1,2,3,4]");
    @SuppressWarnings("unchecked")
    List<Integer> list_=(List<Integer>) parse(listString);
    for (int i:list_){System.out.println(i);}
    String mapString =new String("{'a':'1', 'b':'2', 'c':'3'}");
    @SuppressWarnings("unchecked")
    Map<String,String> map_=(Map<String,String>) parse(mapString);
    //    for (int i:map_){System.out.println(i);}
    System.out.println(map_);
    }
*/



    public static String now(){
    return now("yyyy-MM-dd");
    }
    public static String now(String format){
    /*
    DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    DATE_FORMAT_SHORT = "yyyy-MM-dd HH:mm";
    DATE_FORMAT_SHORTER = "yyyy-MM-dd";
    */
    return CommonUtil.now(format);
    }

    public static String getRandomString(int length) {
    return CommonUtil.getRandomString(length);
    }

	/**
	 * 解析 JSON 为 Map 或 List
	 *
	 * @param str JSON 字符串
	 * @return Map 或 List
	 */
	public static Object parse(String str) {
		return new FMS(str).parse();
	}

	/**
	 * 解析 JSON 字符串为 Map
	 *
	 * @param str JSON 字符串
	 * @return Map
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> parseMap(String str) {
		return (Map<String, Object>) parse(str);
	}

	/**
	 * 解析 JSON 字符串转换为 Bean 对象
	 *
	 * @param json JSON 字符串
	 * @param clz  Bean 对象类引用
	 * @return Bean 对象
	 */
	public static <T> T parseMapAsBean(String json, Class<T> clz) {
		Map<String, Object> map = parseMap(json);
		return MapTool.map2Bean(map, clz, true); // 应该为 false
	}

	/**
	 * 解析 JSON 字符串为 List
	 *
	 * @param str 字符串
	 * @return List
	 */
	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> parseList(String str) {
		return (List<Map<String, Object>>) parse(str);
	}

	/**
	 * 对 Object 尝试类型检测，将其合适地转换为 JSON 字符串返回。
	 *
	 * @param obj 任意对象
	 * @return JSON 字符串
	 */
	public static String toJson(Object obj) {
		if (obj == null)
			return null;
		else if (obj instanceof Boolean || obj instanceof Number)
			return obj.toString();
		else if (obj instanceof String)
			return '\"' + obj.toString().replace("\\", "\\\\").replace("\"", "\\\"").replace("\n", "\\n").replace("\r", "\\r") + '\"';
		else if (obj instanceof String[])
			return jsonArr((String[]) obj, v -> "\"" + v + "\"");
		else if (obj.getClass() == Integer[].class)
			return jsonArr((Integer[]) obj, v -> v + "");
		else if (obj.getClass() == int[].class) {
			Integer[] arr = Arrays.stream((int[]) obj).boxed().toArray(Integer[]::new);
			return jsonArr(arr, v -> v + "");
		} else if (obj instanceof Long[])
			return jsonArr((Long[]) obj, v -> v.toString());
		else if (obj instanceof long[]) {
			Long[] arr = Arrays.stream((long[]) obj).boxed().toArray(Long[]::new);
			return jsonArr(arr, v -> v.toString());
		} else if (obj instanceof Date)
			return '\"' + CommonUtil.simpleDateFormatFactory(CommonUtil.DATE_FORMAT).format((Date) obj) + '\"';
		else if (obj instanceof Map)
			return stringifyMap((Map<?, ?>) obj);
		else if (obj instanceof Map[])
			return jsonArr((Map<?, ?>[]) obj, JsonHelper::stringifyMap);
		else if (obj instanceof BaseModel)
			return beanToJson((BaseModel) obj);
		else if (obj instanceof BaseModel[])
			return jsonArr((BaseModel[]) obj, JsonHelper::beanToJson);
		else if (obj instanceof List) {
			List<?> list = (List<?>) obj;

			if (list.size() > 0) {
				if (list.get(0) instanceof Integer)
					return toJson(list.toArray(new Integer[list.size()]));
				else if (list.get(0) instanceof String)
					return toJson(list.toArray(new String[list.size()]));
				else if (list.get(0) instanceof Map) // Map 类型的输出
					return toJson(list.toArray(new Map[list.size()]));
				else if (list.get(0) instanceof BaseModel) // Bean
					return toJson(list.toArray(new BaseModel[list.size()]));
			} else
				return "[]";
		} else if (obj instanceof Object[])
			return jsonArr((Object[]) obj, JsonHelper::toJson);
		else if (obj instanceof Object) { // 普通 Java Object
			List<String> arr = new ArrayList<>();

			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);

				String key = field.getName();
				if (key.indexOf("this$") != -1)
					continue;

				Object _obj = null;

				try {
					_obj = field.get(obj);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}

				arr.add('\"' + key + "\":" + toJson(_obj));
			}

			return '{' + String.join(",", arr) + '}';
		}

		return null;
	}

	/**
	 * 输入任意类型数组，在 fn 作适当的转换，返回 JSON 字符串
	 *
	 * @param o  数组
	 * @param fn 元素处理器，返回元素 JSON 字符串
	 * @return 数组的 JSON 字符串
	 */
	public static <T> String jsonArr(T[] o, Function<T, String> fn) {
		if (o.length == 0)
			return "[]";

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < o.length; i++) {
			sb.append(fn.apply((T) o[i]));
			if (i != (o.length - 1))
				sb.append(", ");
		}

		return '[' + sb.toString() + ']';
	}

	/**
	 * List 专为 JSON 字符串
	 *
	 * @param list 列表
	 * @param fn   元素处理器，返回元素 JSON 字符串
	 * @return 列表的 JSON 字符串
	 */
	static <T> String eachList(List<T> list, Function<T, String> fn) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < list.size(); i++) {
			sb.append(fn.apply(list.get(i)));
			if (i != (list.size() - 1))
				sb.append(", ");
		}

		return '[' + sb.toString() + ']';
	}

	/**
	 * 输入一个 Map，将其转换为 JSON Str
	 *
	 * @param map 输入数据
	 * @return JSON 字符串
	 */
	private static String stringifyMap(Map<?, ?> map) {
		if (map == null)
			return null;

		if (map.size() == 0)
			return "{}";

		List<String> arr = new ArrayList<>();
		for (Object key : map.keySet())
			arr.add('\"' + key.toString() + "\":" + toJson(map.get(key)));

		return '{' + String.join(",", arr) + '}';
	}

	/**
	 * Bean 转换为 JSON 字符串
	 *
	 * 传入任意一个 Java Bean 对象生成一个指定规格的字符串
	 *
	 * @param bean bean对象
	 * @return String "{}"
	 */
	private static String beanToJson(Object bean) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		PropertyDescriptor[] props = null;

		try {
			props = Introspector.getBeanInfo(bean.getClass(), Object.class).getPropertyDescriptors();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}

		if (props != null) {
			for (int i = 0; i < props.length; i++) {
				try {
					String name = "\"" + props[i].getName() + "\"";
					String value = toJson(props[i].getReadMethod().invoke(bean));

					json.append(name);
					json.append(":");
					json.append(value);
					json.append(",");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			json.setCharAt(json.length() - 1, '}');
		} else
			json.append("}");

		return json.toString();
	}

	/**
	 * 格式化 JSON，使其美观输出到控制或其他地方 请注意 对于json中原有\n \t 的情况未做过多考虑 得到格式化json数据 退格用\t 换行用\r
	 *
	 * @param json 原 JSON 字符串
	 * @return 格式化后美观的 JSON
	 */
	public static String format(String json) {
		int level = 0;
		StringBuilder str = new StringBuilder();

		for (int i = 0; i < json.length(); i++) {
			char c = json.charAt(i);

			if (level > 0 && '\n' == str.charAt(str.length() - 1))
				str.append(CommonUtil.repeatStr("\t", "", level));

			switch (c) {
			case '{':
			case '[':
				str.append(c + "\n");
				level++;

				break;
			case ',':
				if (json.charAt(i + 1) == '"')
					str.append(c + "\n"); // 后面必定是跟着 key 的双引号，但 其实 json 可以 key 不带双引号的

				break;
			case '}':
			case ']':
				str.append("\n");
				level--;
				str.append(CommonUtil.repeatStr("\t", "", level));
				str.append(c);
				break;
			default:
				str.append(c);
				break;
			}
		}

		return str.toString();
	}

	/**
	 * 删除 JSON 注释
	 *
	 * @param str 待处理的字符串
	 * @return 删除注释后的字符串
	 */
	public static String removeComemnt(String str) {
		return str.replaceAll("\\/\\/[^\\n]*|\\/\\*([^\\*^\\/]*|[\\*^\\/*]*|[^\\**\\/]*)*\\*+\\/", "");
	}

	/**
	 * 输出到 JSON 文本时候的换行
	 *
	 * @param str JSON 字符串
	 * @return 转换后的字符串
	 */
	public static String jsonString_covernt(String str) {
		return str.replace("\r\n", "\\n");
	}

	/**
	 * 转义注释和缩进
	 *
	 * @param str JSON 字符串
	 * @return 转换后的字符串
	 */
	public static String javaValue2jsonValue(String str) {
		return str.replaceAll("\"", "\\\\\"").replaceAll("\t", "\\\\\t");
	}

	/**
	 * 是否引号字符串，JSON 支持 " 和 ' 两种的字符串字面量
	 *
	 * @param c 传入的字符
	 * @return 是否引号字符串
	 */
	private static boolean hasQuoataion(char c) {
		return c != '\"' && c != '\'';
	}

	/**
	 * 这是一个更加的 json 解析方法 就算是{'a:a{dsa}':"{fdasf[dd]}"} 这样的也可以处理
	 * 当然{a:{b:{c:{d:{e:[1,2,3,4,5,6]}}}}}更可以处理
	 *
	 * @param jsonStr 合法格式的 json 字符串
	 * @return 有可能 map 有可能是 list
	 */
	public static Object json2Map(String jsonStr) {
		if (CommonUtil.isEmptyString(jsonStr))
			return null;

		Stack<Map<String, Object>> maps = new Stack<>(); // 用来保存所有父级对象
		Stack<List<Object>> lists = new Stack<>(); // 用来表示多层的list对象
		Stack<Boolean> isList = new Stack<>();// 判断是不是list
		Stack<String> keys = new Stack<>(); // 用来表示多层的key

		boolean hasQuoataion = false; // 是否有引号
		String keytmp = null;
		Object valuetmp = null;
		StringBuilder sb = new StringBuilder();

		char[] cs = jsonStr.toCharArray();

		for (int i = 0; i < cs.length; i++) {
			char c = cs[i];

			if (c == ' ') // 忽略空格
				continue;

			if (hasQuoataion) {
				if (hasQuoataion(cs[i]))
					sb.append(cs[i]);
				else
					hasQuoataion = false;

				continue;
			}

			switch (cs[i]) {
			case '{': // 如果是 { map 进栈

				maps.push(new HashMap<String, Object>());
				isList.push(false);
				continue;
			case '\'':
			case '\"':
				hasQuoataion = true;
				continue;
			case ':':// 如果是：表示这是一个属性建，key 进栈

				keys.push(sb.toString());
				sb = new StringBuilder();
				continue;
			case '[':

				lists.push(new ArrayList<Object>());
				isList.push(true);
				continue;
			case ',':
				/*
				 * 这是一个分割，因为可能是简单地 string 的键值对，也有可能是 string=map 的键值对，因此 valuetmp 使用 object 类型；
				 * 如果 valuetmp 是 null 应该是第一次，如果 value 不是空有可能是 string，那是上一个键值对，需要重新赋值 还有可能是 map
				 * 对象，如果是 map 对象就不需要了
				 */
				if (sb.length() > 0)
					valuetmp = sb.toString();

				sb = new StringBuilder();
				boolean listis = isList.peek();

				if (!listis) {
					keytmp = keys.pop();

					if (valuetmp instanceof String)
						maps.peek().put(keytmp, MappingValue.toJavaValue(valuetmp.toString())); // 保存 Map 的 Value
					else
						maps.peek().put(keytmp, valuetmp);
				} else
					lists.peek().add(valuetmp);

				continue;
			case ']':

				isList.pop();

				if (sb.length() > 0)
					valuetmp = sb.toString();

				sb = new StringBuilder();
				lists.peek().add(valuetmp);
				valuetmp = lists.pop();

				continue;
			case '}':

				isList.pop();
				// 这里做的和，做的差不多，只是需要把 valuetmp=maps.pop(); 把 map 弹出栈
				keytmp = keys.pop();

				if (sb.length() > 0)
					valuetmp = sb.toString();

				sb = new StringBuilder();
				maps.peek().put(keytmp, valuetmp);
				valuetmp = maps.pop();

				continue;
			default:
				sb.append(cs[i]);
				continue;
			}

		}

		return valuetmp;
	}
}