//java版本信息
C:\Users>java -version
java version "1.8.0_271"
Java(TM) SE Runtime Environment (build 1.8.0_271-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.271-b09, mixed mode)

//java以及数据库驱动安装信息参考
//D:\JAVA\bin\javac.exe
//D:\JAVA\jre\lib\ext\sqlite-jdbc-3.8.6.jar
//jar包下载：https://search.maven.org/artifact/org.xerial/sqlite-jdbc

//命令参考
D:\JAVA\bin\javac.exe *java -encoding UTF-8
D:\JAVA\bin\java.exe cn.edu.jcu.coder.code.Main

运行：
    路径:\src\main\java>  命令java.exe cn.edu.jcu.coder.code.Main
编译：
    路径：\src\main\java\cn\edu\jcu\coder\code>  命令javac.exe *java -encoding UTF-8
    路径：\src\main\java\cn\edu\jcu\coder\code\tool>  命令javac.exe *java -encoding UTF-8

/****************************************************************/
⚠注意⚠：
第一次编译，需要在code和code\tool目录下分别运行编译命令，javac不会自动寻找路径
运行路径在\src\main\java，包名限制如此

①src\main\java\cn\edu\jcu\coder\code\tool>       D:\JAVA\bin\javac.exe -d ./.. *java -encoding UTF-8
②src\main\java\cn\edu\jcu\coder\code\tool>       D:\JAVA\bin\javac.exe *java -encoding UTF-8
③src\main\java\cn\edu\jcu\coder\code>            D:\JAVA\bin\javac.exe *java -encoding UTF-8
④src\main\java>                                  D:\JAVA\bin\java.exe cn.edu.jcu.coder.code.Main

①:③的前提
②:④的前提
③:执行所有编译后，再修改code目录下文件后只需执行这个
④：运行Main程序
/****************************************************************/
